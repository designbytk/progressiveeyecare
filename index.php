<?php

// $f3=require('lib/base.php');

// Switched to Composer require for Packages including F3
//For 3rd Party Composer libraries i.e. Facebook Graph-SDK

require_once __DIR__ . '/vendor/autoload.php'; // change path as needed

$f3 = Base::instance();
 
$f3->set('AUTOLOAD','app/controllers/');


$f3->set('DEBUG',1);
//$f3->set('site','http://localhost:3000'); 
$f3->set('site','https://'.$_SERVER["SERVER_NAME"]); 

function($f3) {
		//var_dump( \Web::instance()->request('https://reviews.solutionreach.com/vs/progressive_eye_care_farmington'));

		//$html = Web::instance()->request('https://reviews.solutionreach.com/vs/progressive_eye_care_farmington');
		$reviewsrc = Web::instance()->request('https://reviews.solutionreach.com/vs/reviews/progressive_eye_care_farmington?limit=50'); //iframe Source
		$content = $reviewsrc['body'];
		//echo $content;
		//Hides unclean xhtml error message
		libxml_use_internal_errors(true);
		$dom = new DOMDocument;
		$dom->loadHTML($content);
		//echo "The element whose id is 'php-basics' is: " . $dom->getElementById('reviews')->nodeValue . "\n";
		$xpath = new DOMXpath($dom);
		$reviews = '';
		foreach ($xpath->evaluate('//div[@id="content-container"]') as $p) { //iframe Source @id="content-container"
  				$reviews .= $dom->saveHtml($p);
				}
		echo $reviews;

		echo Template::instance()->render("gui/reviews.html");
	};



$f3->route('GET /', 
  function($f3) {
  	$f3->set('currentpage','home');
  	$f3->set('menu',array('about','services','articles','contact'));
		//** Updated for 3.6 **// 
		//	echo \Template::instance()->render('pages/home.html');
  	echo Template::instance()->render('pages/home.html');
  }
);

$f3->route('GET /home', 
  function($f3) {
    $f3->reroute('/');
  }
);

$f3->route('GET /@page',
  function($f3,$page) {
  	$page = $f3->get('PARAMS.page');
  	$f3->set('currentpage',$page);

	$reviewsrc = Web::instance()->request('https://reviews.solutionreach.com/vs/reviews/progressive_eye_care_farmington?limit=50'); //iframe Source
	$content = $reviewsrc['body'];

	libxml_use_internal_errors(true);
	$dom = new DOMDocument;
	$dom->loadHTML($content);
	$xpath = new DOMXpath($dom);
	$reviews = '';
	foreach ($xpath->evaluate('//div[@id="content-container"]') as $p) { //iframe Source @id="content-container"
			$reviews .= $dom->saveHtml($p);
			}
	
		//echo $reviews;
		//$f3->set('review',$reviews);	
   
    if ($page == "articles") {
	$f3->set('reviews',$reviews);
    $f3->set('menu',array('home','services','contact','about'));
      	echo Template::instance()->render("pages/{$page}.html");
//        echo Template->serve("pages/{$page}.html");
       }
       elseif ($page == "services") {
	$f3->set('reviews',$reviews);
    $f3->set('menu',array('home','articles','contact', 'about'));
      	echo Template::instance()->render("pages/{$page}.html");
      	}
      	elseif ($page == "about") {
		$f3->set('reviews',$reviews);
      	$f3->set('menu',array('home','services','articles','contact'));
      	  	echo Template::instance()->render("pages/{$page}.html");
      	} 
      	elseif ($page == "forms") {
			  $f3->set('reviews',$reviews);
      	$f3->set('menu',array('home','services','contact','about'));
      	  	echo Template::instance()->render("pages/{$page}.html");
      	} else {
			  $f3->set('reviews',$reviews);
      		$f3->set('menu',array('home','services','articles','about'));
      		$f3->set('is_sent','');
      		echo Template::instance()->render("pages/{$page}.html");
  	};
	
  }
);

$f3->route('GET /articles/@article', 
  function($f3) {
  	
  	  $article = $f3->get('PARAMS.article');
	  $f3->set('currentarticle','pages/articles/'.$article.'.html');
	  $f3->set('currentpage','articles');
  	  $f3->set('menu',array('home','services','contact','about'));
      echo Template::instance()->render("pages/article.html");
	}
);

//WIP *** Reviews from remote url *** //
$f3->route('GET /beta/reviews',
	function($f3) {
		//var_dump( \Web::instance()->request('https://reviews.solutionreach.com/vs/progressive_eye_care_farmington'));

		//$html = Web::instance()->request('https://reviews.solutionreach.com/vs/progressive_eye_care_farmington');
		$reviewsrc = Web::instance()->request('https://reviews.solutionreach.com/vs/reviews/progressive_eye_care_farmington?limit=50'); //iframe Source
		$content = $reviewsrc['body'];
		//echo $content;
		//Hides unclean xhtml error message
		libxml_use_internal_errors(true);
		$dom = new DOMDocument;
		$dom->loadHTML($content);
		//echo "The element whose id is 'php-basics' is: " . $dom->getElementById('reviews')->nodeValue . "\n";
		$xpath = new DOMXpath($dom);
		$reviews = '';
		foreach ($xpath->evaluate('//div[@id="content-container"]') as $p) { //iframe Source @id="content-container"
  				$reviews .= $dom->saveHtml($p);
				}
		echo $reviews;

		echo Template::instance()->render("gui/reviews.html");
	}
);

//WORK IN PROGRESS! works 10/8
$f3->route('POST /contact/send', 
  function($f3) {
  
  			$name = $f3->get('POST.name');
  			$email = $f3->get('POST.email'); 
  			$msg = $f3->get('POST.message');
  			$msg = 'A message from '.$name.': '."\r".'================================='."\r".$msg."\r".'================================='."\r".'Email: '.$email;  			
  			  
  // instantiate smtp object
              $mail = new SMTP('juliet.unisonplatform.com',465,'SSL','site@pecct.com','farmington');
              $mail->set('from','"pecct.com" <site@pecct.com>');
              $mail->set('to','<progressiveeyecare2@yahoo.com>');
              $mail->set('subject','Website Message from: '.$name);
              $mail->send($msg);
             
	}
);

// Facebook Integration
$f3->route('GET /beta/facebook-integration',
	function($f3) {
		

		echo Template::instance()->render("src/templates/facebook.html");
	}
);


$f3->route('GET /beta/facebook-class','FacebookData->index');


//$f3->run($f3);
$f3->run();


?>