var gulp  = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    cleanCss = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    postcss      = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync').create()


    

gulp.task('sass', function() {
  return gulp.src(['src/scss/*.scss'])
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([ autoprefixer({ browsers: [
      'Chrome >= 35',
      'Firefox >= 38',
      'Edge >= 12',
      'Explorer >= 10',
      'iOS >= 8',
      'Safari >= 8',
      'Android 2.3',
      'Android >= 4',
      'Opera >= 12']})]))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('css/'))
    .pipe(cleanCss())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('css/'))
});

gulp.task('scripts', function() {
  return gulp.src(['resources/js/**/*.js'])
      .pipe(uglify())
      .pipe(gulp.dest('dist/js/'))
});


gulp.task('serve', function() {
  browserSync.init({
      server: './dist/'
  });
  gulp.watch('./src/scss/**/*.scss', ['build-theme']);
  gulp.watch('./**/*.html').on('change', browserSync.reload);
});

gulp.task( 'render', function() {
  return gulp.src(['resources/nunjucks/index.html'])
      .pipe(nunjucks.compile())
      .pipe( gulp.dest('dist/') ) ;
});




gulp.task('watch', ['build-theme','scripts'], function() {
  gulp.watch(['resources/scss/*.scss','resources/js/*.js'], ['build-theme','scripts']);
});

gulp.task('default', ['sass','scripts','render'], function() {
});



