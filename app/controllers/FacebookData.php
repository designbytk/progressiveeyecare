<?php 

class FacebookData {
  function index(){
    // echo 'hello from index';
    
    //Facebook Graph function
    // require_once __DIR__ . '/vendor/autoload.php'; // change path as needed

    $fb = new \Facebook\Facebook([
      'app_id' => '456951404755376',
      'app_secret' => 'b23529c3cb77d6ffb22c5b034501ec4f',
      'default_graph_version' => 'v2.10',
      //'default_access_token' => '{access-token}', // optional
    ]);

    // Use one of the helper classes to get a Facebook\Authentication\AccessToken entity.
    //   $helper = $fb->getRedirectLoginHelper();
    //   $helper = $fb->getJavaScriptHelper();
    //   $helper = $fb->getCanvasHelper();
    //   $helper = $fb->getPageTabHelper();

    try {
      // Get the \Facebook\GraphNodes\GraphUser object for the current user.
      // If you provided a 'default_access_token', the '{access-token}' is optional.
      $response = $fb->get('/me', 'EAACEdEose0cBAD2durLdn27myAgmcQNiCC9ZAzfAQbkZB3jzOW8ioZCYSSdrE4cih6MFwpJiq4xr1BKeeLanxU02xwTPyIAYEiRNlaE3c25mDZAdepvEGPmTYnzMswGZA6sPM01lFqSwsB4foOjLngzCnsnMlP9nCPO2WnNgVnoocX3DyJbG1ZBjEGlCyHjA2RBnZBmTZCMi1gZDZD');
    } catch(\Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(\Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $me = $response->getGraphUser();
    echo 'Logged in as ' . $me->getName();

    echo Template::instance()->render("src/templates/facebook.html");
  }
}