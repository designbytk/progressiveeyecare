<section>
<article class="full">
<h1>Contact Lens Basics</h1>
<p>Contact lenses, like eyeglasses or vision surgery, can correct nearsightedness, farsightedness and astigmatism. About 20 percent of Americans who need vision correction for these refractive errors wear contact lenses.</p>
<p>While some people enjoy making a fashion statement with eyeglasses, others prefer their appearance without them. Contact lenses offer the ability to be glasses-free without expensive vision surgery. Contacts also provide a wider field-of-view than glasses, which is great for driving and sports.</p>
<p>Contact lenses have been around for more than a hundred years, and today just about everyone can wear contact lenses. If you were told in the past that you couldn't wear contacts, odds are you can today. There are more convenient and healthy contact lens options than ever, including many contact lenses that can correct astigmatism.</p>
<p>The first step in being fitted for contacts is to see an eye doctor for a comprehensive eye exam. In the United States, contact lenses are considered medical devices and must be prescribed and properly fitted by an eye care professional (ECP). Your ECP will evaluate your visual needs, your eye structure, and your tears to help determine the best type of contact lenses for you.</p>
<p>Contact lenses are classified according to:</p>
<p>The lens material</p>
<p>The lens design</p>
<p>How long you can wear them without removal</p>
<p>How long you can use them before they should be discarded</p>
<p><b>Contact Lens Materials</b></p>
<p>There are four types of contact lenses, based ion the material they are made of:</p>
<p><b>Soft lenses</b> are thin lenses made of gel-like, water-containing plastics. More than 90 percent of contact lenses worn today are soft lenses. They generally are easy to adapt to and cover the entire cornea (the clear front surface of your eye).</p>
<p><b>Gas permeable lenses</b> (also known as GP, RGP or rigid gas permeable lenses) are smaller lenses made from rigid, waterless plastics. In many cases, GP lenses provide sharper vision than soft lenses.</p>
<p><b>Hybrid lenses</b> have a central GP zone, surrounded by a border made of a soft lens material. These lenses provide the crisp optics of a GP lens, with comfort that rivals soft lenses.</p>
<p><b>Hard lenses</b> are similar in appearance to GP lenses, but they are made of rigid plastic that is not permeable to oxygen. Hard lenses have virtually been replaced by GP lenses and rarely are prescribed today.</p>
<p>The most popular contact lenses worn today are a special type of soft lens called silicone hydrogel lenses. These lenses allow more oxygen to pass through them than conventional soft lenses, reducing the risk of contact lens discomfort and complications.</p>
<p><b>Contact Lens Wearing Time</b></p>
<p>There are two types of contact lenses based on recommended wearing time:</p>
<p><b>Daily wear contacts</b> — Lenses that must be removed nightly</p>
<p><b>Extended wear contacts</b> — Lenses that can be worn overnight</p>
<p>"Continuous wear" is a term that's sometimes used to describe 30 consecutive nights of lens wear — the maximum wearing time approved by the FDA for certain brands of extended wear lenses.</p>
<p><b>Contact Lens Replacement Frequency</b></p>
<p>Even with proper care, contact lenses (especially soft contacts) should be replaced frequently to prevent the build-up of lens deposits and contamination that increase the risk of eye infections.</p>
<p>Soft lenses have these general classifications, based on how frequently they should be discarded:</p>
<p><b>Daily disposable</b> — Discard after a single day of wear</p>
<p><b>Disposable</b> (used for daytime wear) — Discard after two weeks</p>
<p><b>Disposable</b> (used for overnight wear) — Discard after one week</p>
<p><b>Continuous wear</b> (used for 30-day wear) — Discard monthly</p>
<p><b>Planned replacement</b> — Discard at intervals of one to three months</p>
<p>Gas permeable contact lenses are more resistant to lens deposits and do not need to be discarded as frequently as soft lenses. Often, GP lenses can last a year or longer before they need to be replaced.</p>
<p><b>Contact Lens Designs</b></p>
<p>Several contact lens designs are available to correct various types of vision problems:</p>
<p><b>Spherical</b> contact lenses are the most common design. Spherical soft lenses correct nearsightedness and farsightedness. Spherical GP lenses can correct nearsightedness, farsightedness and astigmatism.</p>
<p><b>Toric </b>lenses (soft and GP) have multiple lens powers to correct astigmatism.</p>
<p><b>Bifocal</b> <b>and multifocal </b>contact lenses (soft and GP) contain different zones for near and far vision to correct presbyopia.</p>
<p><b>Orthokeratology</b> (ortho-k) and corneal refractive therapy (CRT) lenses are specialty GP lenses designed to reshape the cornea during sleep and temporarily correct myopia and other refractive errors for clear vision without glasses or contacts during the day.</p>
<p>Custom soft and GP lens designs also are available for hard-to-fit eyes, including eyes with keratoconus.</p>
<p><b>More Contact Lens Features</b></p>
<p><b>Colored Lenses.</b> Soft contact lenses are available in a variety of colors that can enhance the natural color of your eyes — to make your green eyes even greener, for example. Other colored lenses can change the color of your eyes entirely — from brown to blue, for example.</p>
<p><b>Special-Effect Lenses.</b> Also called theatrical, gothic, Halloween or costume lenses, these soft lenses take coloration one step further to make you look like a cat, a zombie, or another alter-ego of your choice.</p>
<p><b>Prosthetic Lenses.</b> These custom-made color contact lenses are used to restore a natural appearance to eyes that have been disfigured by injury or disease. In cases when only one eye is affected, a prosthetic lens is designed to closely match the appearance of the normal eye.</p>
<p><b>Which Contact Lens Is Right for You?</b></p>
<p>The first step in finding the best contacts for you is to schedule a comprehensive eye exam and contact lens consultation with your eye doctor. During this exam, your doctor will make sure your eyes are healthy enough to wear contact lenses and advise you regarding what to expect when wearing contacts.</p>
<p>Next is the contact lens fitting itself. Detailed measurements of your eyes are taken, and trial lenses often are applied to achieve the best possible fit and determine if you can comfortably wear contacts.</p>
<p>A contact lens fitting takes several office visits and you will be asked to return a number of times to make sure the lenses continue to fit properly and remain comfortable after prolonged periods of wear. In some cases, changes of lens size or design are needed before the fitting process is complete.</p>
<p>Your prescription for contact lenses is written only after the contact lens fitting process is completed and your doctor is satisfied with the long-term fit of your lenses and how well your eyes tolerate contact lens wear.</p>
<p><b>Contact Lens Care</b></p>
<p>Caring for your contact lenses — cleaning, disinfecting and storing them — is much easier than it used to be. In most cases today, only a single care solution is required for cleaning, rinsing and storing your lenses. And if you choose daily disposable soft lenses, routine lens care can be eliminated altogether.</p>
<p>Your eye doctor or contact lens technician will teach you how to apply, remove and care for your lenses during your contact lens fitting.</p>
<p><i>Source:  </i><span class="s2"><i>Contact Lens Basics by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p>Article ©2011 Access Media Group LLC.All rights reserved.Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
    <li><a href="/articles/contacts-bifocal">Bifocal and Multifocal Contact Lenses</a></li>
    <li><a href="/articles/contacts-hardtofit">Contact Lenses for the 'Hard-to-Fit' Patient</a></li>
    <li><a href="/articles/contacts-gp">Gas Permeable (GP) Contact Lenses</a></li>
    <li><a href="/articles/contacts-orthok" class="last">Orthokeratology</a></li>
</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>