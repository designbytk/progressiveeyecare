<section>
<article class="full">
<h1>Amblyopia (Lazy Eye)</h1>
<p>Amblyopia is reduced vision in an eye caused by abnormal visual development. Commonly called “lazy eye,” amblyopia usually occurs in just one eye, but both eyes can be affected.</p>
<p>Left untreated, amblyopia can cause legal blindness in the affected eye. About 2 to 3 percent of the American population has amblyopia.</p>
<p><b>What causes amblyopia?</b></p>
<p>The most common cause of amblyopia is strabismus, which is misalignment of the eyes. To avoid double vision caused by strabismus, the visual part of the brain suppresses visual information provided by one eye, causing that eye to be amblyopic. </p>
<p>Another cause of amblyopia is a significant difference in the refractive errors (nearsightedness, farsightedness and/or astigmatism) in the two eyes.</p>
<p>It's important to correct amblyopia as early as possible to enable proper visual development and normal visual acuity in both eyes.</p>
<p><b>Amblyopia signs and symptoms</b></p>
<p>Amblyopia generally starts at birth or during early childhood. Because the vision in one eye usually remains normal, sometimes there are no symptoms of amblyopia until the "good" eye is covered.</p>
<p>If amblyopia is caused by strabismus, it is the misalignment of the eyes that often leads to the diagnosis of amblyopia after vision testing of each eye is performed.</p>
<p><b>Treatment of amblyopia</b></p>
<p>If amblyopia is caused only by unequal refractive error, sometimes full-time wear of glasses or contact lenses will be sufficient for vision to develop properly in the amblyopic eye. But in most cases, eye patching or some other technique to temporarily reduce the visual acuity of the "good" eye is needed to stimulate the visual development of the amblyopic eye.</p>
<p>Eye patching may be required for several hours each day or even all day long, and may continue for weeks or months. If you have a lot of trouble with your child taking the patch off, you might consider a prosthetic contact lens that is specially designed to block vision in one eye and is colored to closely match the other eye.</p>
<p>Another alternative to patching is the use of atropine eye drops in the non-amblyopic eye. The drops blur the vision of the good eye to force greater use of the amblyopic eye. Studies have shown use of atropine eye drops is comparable to eye patching for treating amblyopia and doesn't require constant vigilance to make sure your child wears an eye patch.</p>
<p>In cases when the amblyopia is caused by a large eye turn, strabismus surgery is usually required to straighten the eyes. The surgery corrects the muscle problem that causes strabismus so the eyes can focus together and see properly.</p>
<p>In many cases, a program of active vision therapy also is recommended to speed the development of normal vision and visual skills in an eye with amblyopia. Vision therapy exercises the eyes and helps both eyes work as a team. Vision therapy for someone with amblyopia forces the brain to use the amblyopic eye, thus restoring vision.</p>
<p>Amblyopia does not go away on its own, and untreated amblyopia can lead to permanent visual problems and poor depth perception. If your child has amblyopia and his or her "good" eye develops disease or is injured later in life, this could cause a permanent disability.</p>
<p>For best results, amblyopia should be treated as soon as possible during childhood. If amblyopia is detected and aggressively treated before the age of 8 or 9, in many cases normal 20/20 vision can be achieved.</p>
<p><i>Source: Amblyopia (Lazy Eye) by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p class="credits">Article &#169;2011 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
    <li><a href="/articles/conditions-astigmatism">Astigmatism</a></li>
    <li><a href="/articles/conditions-blepharitis">Blepharitis</a></li>
    <li><a href="/articles/conditions-cataracts">Cataracts</a></li>
    <li><a href="/articles/conditions-cvs">Computer Vision Syndrome</a></li>
    <li><a href="/articles/conditions-diabetic">Diabetic Retinopathy</a></li>
    <li><a href="/articles/conditions-dry-eyes">Dry Eye Syndrome</a></li>
    <li><a href="/articles/conditions-allergies">Eye Allergies</a></li>
    <li><a href="/articles/conditions-floaters">Floaters and Spots</a></li>
    <li><a href="/articles/conditions-glaucoma">Glaucoma</a></li>
    <li><a href="/articles/conditions-hyperopia">Hyperopia</a></li>
    <li><a href="/articles/conditions-keratoconus">Keratoconus</a></li>
    <li><a href="/articles/conditions-amd">Macular Degeneration</a></li>
    <li><a href="/articles/conditions-myopia">Myopia</a></li>
    <li><a href="/articles/conditions-ocular-hypertension">Ocular Hypertension</a></li>
    <li><a href="/articles/conditions-pinkeye">Pink Eye (Conjunctivitis)</a></li>
    <li><a href="/articles/conditions-presbyopia">Presbyopia</a></li>
    <li><a href="/articles/conditions-styes" class="last">Stye</a></li>
</ul> 
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>