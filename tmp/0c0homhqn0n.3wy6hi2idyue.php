<section>
<article class="full">
<h1>Learning-Related Vision Problems</h1>
<p>Experts say more than 80 percent of what children are taught in school is presented visually, in the form of books and other written or visual means. So, good vision is essential for learning.</p>
<p>The first step to make sure your child has the proper visual abilities needed in the classroom is to schedule a comprehensive eye exam prior to the school year. During this exam, the eye doctor will make sure your child has 20/20 eyesight and that any nearsightedness, farsightedness and/or astigmatism is fully corrected with glasses or contact lenses.</p>
<p>However, 20/20 visual acuity alone does not mean your child has all the necessary visual skills required for optimal learning.</p>
<b>Good vision is more than 20/20 visual acuity</b>
<p>Visual acuity (how well your child can see letters on a wall chart) is just one aspect of good vision. Your child can have visual acuity of 20/20 and still have vision problems that can affect his or her learning and classroom performance.</p>
<p>Also, many nearsighted kids may have trouble seeing the board in class, but they read exceptionally well and excel in school.</p>
<p>Important visual skills other than visual acuity that are needed for learning include:</p>
<p>&bull; Binocular vision skills – How well your child’s eyes can blend visual images from both eyes into a single, three-dimensional image.</p>
<p>&bull; Eye teaming skills – How well your child’s eyes work together as a synchronized team (to converge for proper eye alignment for reading, for example).</p>
<p>&bull; Eye movement skills – How smoothly and accurately your child can move their eyes across a printed page in a textbook.</p>
<p>&bull; Eye focusing abilities – How well his or her eyes can change focus from far to near and back again (for copying information from the board, for example).</p>
<p>&bull; Visual perceptual skills – How well your child can identify an object in his or her visual field, judge its importance and associate it with previous visual information stored in the brain.</p>
<p>&bull; Visual-motor integration – The quality of your child’s eye-hand coordination, which is important not only for sports, but also for handwriting and the ability to efficiently copy written information from a book or chalkboard.</p>
<p>&bull; Deficiencies in any of these important visual skills can significantly affect your child’s learning ability and school performance.</p>
<b>Many kids have vision problems that affect learning</b>
<p>Many kids have undetected learning-related vision problems.</p>
<p>According to the College of Optometrists in Vision Development (COVD), one study indicates 13 percent of children between the ages of 9 and 13 suffer from moderate to severe convergence insufficiency (an eye teaming problem that can affect reading performance), and as many as one in four school-age children may have at least one learning-related vision problem.</p>
<b>Signs and symptoms of learning-related vision problems</b>
<p>Signs and symptoms of learning-related vision disorders include:</p>
<p>&bull; Frequent headaches or eye strain</p>
<p>&bull; Blurred distance or near vision, particularly after reading or other close work</p>
<p>&bull; Difficulty changing focus from distance to near and back</p>
<p>&bull; Double vision, especially during or after reading</p>
<p>&bull; Avoidance of reading</p>
<p>&bull; Easily distracted when reading</p>
<p>&bull; Loss of place, repetition, and/or omission of words while reading</p>
<p>&bull; Letter and word reversals</p>
<p>&bull; Poor reading comprehension</p>
<p>&bull; Poor handwriting</p>
<p>&bull; Hyperactivity or impulsiveness during class</p>
<p>&bull; Poor overall school performance</p>
<b>Comprehensive children’s eye exam</b>
<p>A comprehensive children’s eye exam includes tests performed in a routine adult eye exam, plus additional tests to detect learning-related vision problems.</p>
<p>These extra tests may include an assessment of eye focusing, eye teaming, and eye movement abilities (also called accommodation, binocular vision, and ocular motility testing).</p>
<p>Also, depending on the type of problems your child is having, other testing or referral to a specialist may be recommended.</p>
<b>Vision therapy</b>
<p>If your child has a learning-related vision problem that cannot be corrected with regular glasses or contact lenses, vision therapy may help. Vision therapy is a program of eye exercises and other activities specifically tailored for each patient to improve their vision skills, including learning-related visual skills. </p>
<b>Vision and learning disabilities</b>
<p>Learning-related vision problems are not the same thing as a learning disability. A child who is struggling in school could have a learning-related vision problem, a learning disability or both.</p>
<p>Vision therapy is a treatment for vision problems; it does not correct a learning disability. However, children with learning disabilities also may have vision problems that are contributing to their difficulties in the classroom.</p>
<p><i>Source: Learning-Related Vision Problems by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a></i></p>
<p class="credits">Article &#169;2011 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
    <li><a href="/articles/children-faq">Children's Vision FAQs</a></li>
    <li><a href="/articles/children-learning" class="last">Learning-Related Vision Problems</a></li>
</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>
