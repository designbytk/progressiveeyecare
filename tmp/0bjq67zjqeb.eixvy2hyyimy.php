<div class="facebook">
  <img alt="facebook, deals, frames"
       src="<?php echo $site; ?>/img/facebook-call-to-action.gif" />
  <h3>
    <em>Visit our Facebook page for current promotions, offers, and to view the latest frame styles.</em>
  </h3>
  <a href="https://www.facebook.com/Progressive-Eye-Care-168939389788564/">Check It Out</a>
</div>
