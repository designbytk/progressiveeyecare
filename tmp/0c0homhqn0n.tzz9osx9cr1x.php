<section>
<article class="full">
<h1>Ocular Hypertension</h1>
<p>Ocular hypertension is elevated intraocular pressure (IOP), which means the pressure inside your eye is higher than normal.</p>
<p>High eye pressure also is associated with glaucoma, which is a more serious condition characterized by optic nerve damage and vision loss. Though ocular hypertension can lead to glaucoma, it also is possible that you can have ocular hypertension that doesn't damage your vision or eyes.</p>
<p>Studies suggest that 2 to 3 percent of the general population may have ocular hypertension.</p>
<p><b>Signs and symptoms of ocular hypertension</b></p>
<p>You can't tell by yourself that you have ocular hypertension, because there are no outward signs or symptoms such as pain or redness. At each eye exam, your eye care practitioner will measure your IOP and compare it to normal levels.</p>
<p>The instrument used to measure eye pressure is called a tonometer. Your eye typically is numbed with eye drops, and a small probe gently rests against your eye's surface. Other tonometers direct a puff of air onto your eye's surface to indirectly measure IOP.</p>
<p><b>What causes ocular hypertension?</b></p>
<p>Anyone can develop ocular hypertension, but it's most common in African-Americans, people over age 40, those with family history of ocular hypertension or glaucoma, and those with diabetes or high amounts of nearsightedness.</p>
<p>High eye pressure can be caused by the body producing too much clear fluid (aqueous) in the eye or inadequate drainage of aqueous from the eye. Certain medications, such as steroids, and trauma can cause higher-than-normal IOP measurements as well.</p>
<p><b>Treatment of ocular hypertension</b></p>
<p>People with ocular hypertension are at increased risk for developing glaucoma, so some eye doctors prescribe medicated eye drops to lower IOP in cases of ocular hypertension.</p>
<p>Because these medications can be expensive and may have side effects, other eye doctors choose to monitor your IOP and only take action if you show signs of developing glaucoma. To prevent vision loss from ocular hypertension, be sure to have your IOP measured at the intervals recommended by your eye doctor.</p>
<p><i>Source: Ocular Hypertension by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p class="credits">Article &#169;2011 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
    <li><a href="/articles/conditions-amblyopia">Amblyopia (Lazy Eye)</a></li>
    <li><a href="/articles/conditions-astigmatism">Astigmatism</a></li>
    <li><a href="/articles/conditions-blepharitis">Blepharitis</a></li>
    <li><a href="/articles/conditions-cataracts">Cataracts</a></li>
    <li><a href="/articles/conditions-cvs">Computer Vision Syndrome</a></li>
    <li><a href="/articles/conditions-diabetic">Diabetic Retinopathy</a></li>
    <li><a href="/articles/conditions-dry-eyes">Dry Eye Syndrome</a></li>
    <li><a href="/articles/conditions-allergies">Eye Allergies</a></li>
    <li><a href="/articles/conditions-floaters">Floaters and Spots</a></li>
    <li><a href="/articles/conditions-glaucoma">Glaucoma</a></li>
    <li><a href="/articles/conditions-hyperopia">Hyperopia</a></li>
    <li><a href="/articles/conditions-keratoconus">Keratoconus</a></li>
    <li><a href="/articles/conditions-amd">Macular Degeneration</a></li>
    <li><a href="/articles/conditions-myopia">Myopia</a></li>
    <li><a href="/articles/conditions-pinkeye">Pink Eye (Conjunctivitis)</a></li>
    <li><a href="/articles/conditions-presbyopia">Presbyopia</a></li>
    <li><a href="/articles/conditions-styes" class="last">Stye</a></li>
</ul> 
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>