<footer class="clearfix">
<div>
<img src="<?php echo $site; ?>/img/pec-logo.jpg" alt="Progressive Eye Care Logo" />
<nav>
	<ul>
		<li><a href="/">Home</a></li>
		<li><a href="/about">About</a></li>
		<li><a href="/services">Services</a></li>
		<li><a href="/articles">Articles</a></li>
		<li><a href="/forms">Forms</a></li>
		<li><a href="/contact">Contact</a></li>
	</ul>
</nav>
<p>Quality eye care professionals conveniently located in Farmington and Southbury</p>
<p class="copy">&copy; 2016 Progressive Eye Care</p>
</div>
</footer>
