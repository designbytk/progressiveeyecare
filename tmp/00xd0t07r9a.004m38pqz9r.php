<?php if ($currentpage=='forms'): ?>

<!DOCTYPE html>
<html lang="en">
<?php echo $this->render('gui/head.html',$this->mime,get_defined_vars()); ?>	

<body>
<?php echo $this->render('gui/header.html',$this->mime,get_defined_vars()); ?>

<figure>
<div>
	<h2><i class="icon-chevron-sign-right"></i>FORMS</h2>
</div>
</figure>	
<section id="content">
<div>
<section>
<article class="full">
	<img src="../img/forms-download.jpg" alt="Forms Download Montage" />
	<h1>Patient Forms</h1>
	<p>To save time on your visit, please download and print these forms and complete them prior to your appointment. We appreciate your cooperation.</p>
	<div class="download">
		<span><a href="http://downloads.pecct.com/pecct-forms.pdf">Download</a></span>
	</div>
	<p class="credits">This PDF requires a free plugin that may have come included with your browser. If you are having difficulties opening this file <a href="http://get.adobe.com/reader/">Click Here</a> to go to Adobe's web site for Acrobat Reader.</p>
</article>
</section>
<aside>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>
</div>
</section>
<?php echo $this->render('gui/footer.html',$this->mime,get_defined_vars()); ?>	

</body>
</html>


<?php else: ?>
<div id="pecctform">
	<img src="../img/forms-download.jpg" alt="Forms Download Montage" />
	<a class="modal_close" href="#"><i class="icon-remove-sign"></i></a>
	<h1>Patient Forms</h1>
	<p>To save time on your visit, please download and print these forms and complete them prior to your appointment. We appreciate your cooperation.</p>
	<p class="download">
		<span><a href="http://downloads.pecct.com/pecct-forms.pdf">Download</a></span>
	</p>
	<p class="credits">This PDF requires a free plugin that may have come included with your browser. If you are having difficulties opening this file <a href="http://get.adobe.com/reader/">Click Here</a> to go to Adobe's web site for Acrobat Reader.</p>
</div>
<?php endif; ?>