<section>
<article class="full">
<h1>Contact Lenses for the "Hard-to-Fit" Patient</h1>
<p>A number of eye conditions can make wearing contact lenses more challenging. These conditions include:</p>
<p>astigmatism</p>
<p>dry eyes</p>
<p>giant papillary conjunctivitis (GPC)</p>
<p>keratoconus</p>
<p>post-refractive surgery (such as LASIK)</p>
<p>presbyopia</p>
<p>If you have any of these conditions, you still might be able to wear contacts quite successfully. It just means you may need to be fitted with lenses specially designed for your condition or "hard-to-fit" eyes.</p>
<p><b>Contact lenses for astigmatism</b></p>
<p>Astigmatism is a very common condition where the curvature of the front of the eye isn’t symmetrical in all meridians (like a baseball). Instead, the cornea is shaped more like a football or the back of a spoon — with one meridian (from 12 o'clock to 6 o'clock, for example) being relatively flat and the meridian 90 degrees away (3 o'clock to 9 o'clock) being significantly steeper.</p>
<p>In most cases, you can still wear contacts successfully if you have astigmatism — you just need a different kind of lens.</p>
<p>Soft contact lenses specially designed to correct astigmatism are called “toric” lenses. Toric soft lenses have different corrective powers in different lens meridians, and design features to prevent the lens from rotating on the eye. These stabilizing features keep the lens powers aligned in front of the proper meridians of the eye for clear vision.</p>
<p>In some cases, toric soft lenses may rotate too much on the eye, causing blur. If this happens, other brands with different stabilizing features can be tried.</p>
<p>If rotation of toric soft lenses continues to be a problem, gas permeable (GP) lenses (with or without a toric design) also can correct astigmatism.</p>
<p><b>Contact lenses for dry eyes</b></p>
<p>Dry eye is perhaps the most common cause of contact lens discomfort. Symptoms of dry eye syndrome include:</p>
<p>a gritty, dry feeling</p>
<p>a burning sensation</p>
<p>a feeling that something is "in" your eye (called a foreign body sensation)</p>
<p>eye redness (especially later in the day)</p>
<p>blurred or fluctuating vision</p>
<p>If you have dry eyes, the first step is to treat the condition. This can be done a number of ways, including frequent use of artificial tears or other eye drops, use of nutritional supplements, and a doctor-performed procedure called punctal occlusion to close ducts in your eyelids that drain tears away from your eyes.</p>
<p>Once the dry eye condition is treated and symptoms are reduced or eliminated, contact lenses can be tried.</p>
<p>Certain soft contact lens materials may work better than others for dry eyes, and there are even some contact lenses specially designed for people with dry eyes. Also, rigid gas permeable lenses sometimes are better than soft lenses for dry eyes since GP lenses don’t dry out the way soft lenses can.</p>
<p>Replacing your contacts more frequently and reducing your wearing time each day also can reduce dry eye symptoms associated with contact lens wear. Removing your contacts and wearing glasses for specific tasks such as extended computer use also can help.</p>
<p><b>Contact lenses for giant papillary conjunctivitis (GPC)</b></p>
<p>Giant papillary conjunctivitis (GPC) is an inflammatory reaction on the inner surface of the eyelids. One cause of GPC is protein deposits on soft contact lenses. (These deposits are from components of your tear film that stick to your lenses and become chemically altered.)</p>
<p>Usually, changing to one-day disposable contact lenses will solve this problem, since you just throw these lenses away at the end of the day before protein deposits can accumulate on them.</p>
<p>Gas permeable lenses also are a good solution, as protein deposits don’t adhere as easily to GP lenses, and lens deposits on GP lenses are more easily removed with daily cleaning.</p>
<p>In some cases of GPC, using a medicated eye drop for a period of time may be required to reduce the inflammation before you can resume wearing contact lenses.</p>
<p><b>Contact lenses for keratoconus</b></p>
<p>Keratoconus is a relatively uncommon eye condition where the cornea becomes thinner and bulges forward. The term “keratoconus” comes from the Greek terms for cornea (“kerato”) and cone-shaped (“conus”). The exact cause of keratoconus remains unknown, but it appears that genetics and oxidative damage from free radicals play a role.</p>
<p>Gas permeable contact lenses generally have been the treatment of choice for mild and moderate keratoconus. Because they are rigid, GP lenses can help contain the shape of the cornea. They also can correct vision problems caused by keratoconus that cannot be corrected with eyeglasses or soft contacts.</p>
<p>In some cases, a soft contact lens is worn under the GP lens for greater comfort. This technique is called “piggybacking.” Another option for some patients is a hybrid contact lens that has a GP center, surrounded by a "skirt" of comfortable soft lens material.</p>
<p><b>Contact lenses after corrective eye surgery</b></p>
<p>Many Americans each year choose to undergo LASIK or other elective refractive surgery to correct their eyesight. Sometimes, vision problems remain after surgery that can’t be corrected with eyeglasses or a second surgical procedure. In these cases, gas permeable contacts often can restore visual acuity and eliminate problems like glare and halos at night.</p>
<p>GP lenses also are used to correct vision problems after cornea transplant surgery, including irregular astigmatism that cannot be corrected with eyeglasses.</p>
<p>GP lenses prescribed after LASIK and cornea transplants sometimes have a special design called “reverse geometry” to better conform to the altered shape of the cornea. The back surface of these lenses is flatter in the center and steeper in the periphery. (This is the opposite of a normal GP lens design, which is steeper in the center and flattens in the periphery.)</p>
<p><b>Contact lenses for presbyopia</b></p>
<p>Presbyopia is the normal loss of ability to focus on near objects that occurs in middle age. Nearly everyone experiences some symptoms of presbyopia by ages 45 to 50.</p>
<p>Today, there are many designs of bifocal and multifocal contact lenses to correct presbyopia. Another option for presbyopia is monovision — wearing a contact lens in one eye for distance vision and a lens in the other eye that has a modified power for near vision.</p>
<p>During your contact lens fitting, your eye doctor can help you decide whether bifocal/multifocal contact lenses or monovision is best for you.</p>
<p><b>Cost of problem-solving contacts</b></p>
<p>Fitting contact lenses to correct or treat any of the above conditions generally takes more time than a regular contact lens fitting. These “hard-to-fit” cases usually require a series of office visits and multiple pairs of trial lenses before the final contact lens prescription can be determined.</p>
<p>Also, special-design contact lenses required for these conditions typically are more costly than regular soft contact lenses. Therefore, fees for these fittings are higher than fees for regular contact lens fittings.</p>
<p><b>Find out if you can wear contact lenses</b></p>
<p>If you are interested in wearing contact lenses, call our office to schedule a consultation. Even if you’ve been told you’re not a good candidate for contacts because you have one of the above conditions or for some other reason, we may be able to help you wear contact lenses safely and successfully.</p>
<p><i>Source: Contacts for Hard-to-Fit Eyes by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p class="credits">Article &#169;2011 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
    <li><a href="/articles/contacts-bifocal">Bifocal and Multifocal Contact Lenses</a></li>
    <li><a href="/articles/contacts-basics">Contact Lens Basics</a></li>
    <li><a href="/articles/contacts-gp">Gas Permeable (GP) Contact Lenses</a></li>
    <li><a href="/articles/contacts-orthok" class="last">Orthokeratology</a></li>
</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>