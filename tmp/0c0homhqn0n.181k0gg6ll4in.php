<section>
<article class="full">
<h1>Your Infant's Visual Development</h1>
<p>The visual system of a newborn infant takes some time to develop. Knowing the expected milestones of your baby’s vision development during their first year of life can help you ensure your infant son or daughter is seeing properly.</p>
<b>During your pregnancy</b>
<p>Your child’s vision development begins before birth. How you care for your own body during your pregnancy is extremely important for the development of your baby’s body and mind, including his or her eyes and the vision centers in the brain.</p>
<p>Be sure to follow the instructions provided by your obstetrician (OB/GYN doctor) regarding good nutrition and the proper amount of rest during your pregnancy. And of course, avoid smoking and consuming alcohol or drugs during pregnancy, as these toxins can cause multiple problems for your baby, including serious vision problems.</p>
<b>At birth</b>
<p>At birth, your baby's vision is quite blurred and comprises only shades of gray. This is because cells in the eyes and brain that control vision aren’t fully developed at this stage.</p>
<p>Also, your infant's eyes don’t yet have the ability to change focus and see close objects clearly. So don’t be concerned if your baby doesn’t seem to be focusing on objects right away, including your face. It just takes time.</p>
<b>The first four weeks</b>
<p>Within the first few weeks of life, your baby begins to see colors. But visual acuity and eye teaming take a bit longer to develop — so if your infant's eyes occasionally look unfocused or misaligned, don't worry.</p>
<p>The eyes of infants are not as sensitive to visible light as adult eyes are, but they need protection from the sun's harmful UV rays. Keep your baby's eyes shaded outdoors with a brimmed cap or some other means.</p>
<b>Weeks eight to 12</b>
<p>At eight to 12 weeks, your baby’s vision and eye teaming skills continue to improve. Your child should be able to follow moving objects at this stage, and start reaching for things he or she sees. Also, infants at this stage are learning how to shift their gaze from one object to another without having to move their head. </p>
<b>Months four to six</b>
<p>By six months of age, significant advances take place in the vision centers of the brain, allowing your infant to see more distinctly, move his or her eyes faster and more accurately, and have a better ability to follow moving objects.</p>
<p>Visual acuity improves to nearly adult level at six months of age, and color vision also should be nearly fully developed as well.</p>
<p>Children also develop better eye-hand coordination at four to six months of age. They’re able to quickly locate and pick up objects, and accurately direct a bottle (and many other things) to their mouth.</p>
<b>Months seven to 12</b>
<p>At this stage, your child should be crawling and quite mobile. By 12 months of age, your young maturing infant also should be getting better at judging distances and more skilled at locating, grasping and throwing objects, too.</p>
<p>Because of the greater mobility young children have at this stage, be sure to watch them closely to keep them from harm as they explore their environment. Keep cabinets that contain cleaning supplies locked, and put a barrier in front of stairwells.</p>
<b>Eye exams for infants and young children</b>
<p>If you suspect something is seriously wrong with your baby’s eyes in their first few months of life (a bulging eye, a red eye, excess tearing, or a constant misalignment of the eyes, for example), call your eye doctor immediately for advice.</p>
<p>For routine eye care for infants and young children, the American Optometric Association (AOA) recommends scheduling your child's first comprehensive eye exam at six months of age. Though your baby can’t yet read letters on a wall chart, your eye doctor can perform non-verbal testing to determine visual acuity, detect excessive or unequal amounts of nearsightedness, farsightedness and astigmatism, and evaluate eye teaming and eye alignment.</p>
<p>Your eye doctor also will check the health of your baby’s eyes, looking for anything that might interfere with normal and continuing vision development.</p>
<p>We welcome providing eye care for even the youngest children. For more information about eye exams for kids or to schedule your child’s first eye exam, please call our office.</p>
<p><span class="s1"><i>Source:  </i></span><span class="s2">Your Infant’s Vision Development</span><span class="s1"><i> by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a></i></span></p>
<p><i>Article ©2011 Access Media Group LLC.All rights reserved.Reproduction other than for one-time personal use is strictly prohibited.</i></p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
    <li><a href="/articles/children-faq">Children's Vision FAQs</a></li>
    <li><a href="/articles/children-learning" class="last">Learning-Related Vision Problems</a></li>
</ul> 
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>
