<section>
<article class="full">
<h1>LASIK: Patient Selection Criteria</h1>
<p>Laser eye surgery isn't for everyone. Here are a few guidelines to help you decide if LASIK is right for you:</p>
<p><b>Are you an adult?</b> You need to be at least 18 years of age to have LASIK. (Younger patients can sometimes be treated as an exception. Discuss this with your surgeon.)</p>
<p><b>Are your eyes healthy?</b> If you have any condition that can affect how your eyes respond to surgery or heal afterwards, wait until that condition is resolved. Examples include chronic dry eyes, conjunctivitis (“pink eye”) and any eye injury. Some conditions, such as cataracts, keratoconus and uncontrolled glaucoma, may disqualify you completely.
</p>
<p><b>Is your vision stable?</b> If your prescription for eyeglasses or contact lenses is still changing year-to-year, you are not a good candidate for LASIK. Make sure your prescription is stable for a 12-month period before having LASIK. Otherwise it's possible your eyes will continue to change after surgery and you again will need glasses or another LASIK procedure to improve your eyesight.</p>
<p></p>
<p><b>Are you pregnant?</b> Hormonal changes during pregnancy can cause swelling in your corneas, changing your vision. Dry eyes are also common when you’re pregnant. Also, eye medications (antibiotics and steroids) used during and after LASIK may be risky for your baby, whether unborn or nursing. Wait a few months after your baby is born before having LASIK.</p>
<p><b>Do you have any systemic and autoimmune disease?</b> Problems like rheumatoid arthritis, diabetes, HIV and AIDS can increase the risk that your eyes might not heal properly after LASIK. Professional opinions vary regarding which diseases are automatic disqualifiers and which ones might pose acceptable LASIK risks. Ask your eye surgeon for details.
</p>
<p><b>Do you have very high amounts of nearsightedness or farsightedness?</b> LASIK works best for mild to moderate amounts of myopia. If you are very nearsighted, it's possible that too much corneal tissue would have to be removed during LASIK to correct your vision, which could put you at risk for serious LASIK complications. In such cases, an alternative refractive procedure such as phakic IOL implantation or refractive lens exchange might be a safer option and produce better outcomes.</p>
<p>Your eye surgeon will discuss these and other selection criteria with you at your LASIK consultation.</p>
<p><i>Source: </i><span class="s1"><i>LASIK Success Criteria – 9 Essential Guidelines by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p class="p7">Article ©2011 Access Media Group LLC.All rights reserved.Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
	<ul>
    <li><a href="/articles/visionsurgery-basics">Corrective Eye Surgery Basics</a></li>
    <li><a href="/articles/visionsurgery-lasik">LASIK</a></li>
    <li><a href="/articles/visionsurgery-risks">LASIK Risks and Complications</a></li>
    <li><a href="/articles/visionsurgery-prk" class="last">PRK: The Original Laser Eye Surgery</a></li>
</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>