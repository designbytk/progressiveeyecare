<!DOCTYPE html>
<html lang="en">
<?php echo $this->render('gui/head.html',$this->mime,get_defined_vars()); ?>	

<body>
<?php echo $this->render('gui/header.html',$this->mime,get_defined_vars()); ?>
<figure>
<div>
	<h2><i class="icon-chevron-sign-right"></i>OUR PRACTICE</h2>
</div>
</figure>	
<section id="content">

<div>
<!-- SEO addition 5/21/2016 -->
<article class="fullstory">
	<p><em>Progressive Eye Care</em> is a group of Optometrists and Opticians with the experience, dedication and personal attention to handle children and adult vision care. We are here to address your needs - from eye nutrition, to glasses, to contact lenses and lasik. Capable of fitting you with the latest eyeglass styles including European designs, sport and sun performance styles, Ray Bans and other popular brands.</p>
	<p>We have been serving the Greater Hartford Connecticut Area, including Farmington, West Hartford, Avon, Canton, Unionville, Simsbury, Bristol, Plainville and Southington, since 1991. Welcoming patients from Litchfield County, Fairfield County and New Haven County as well.</p>
	<p>Eye doctor appointments with us are readily available and we participate with most insurance plans. <a href="/contact">Contact us</a> today.</p>
</article>
<!-- END addition -->
<section>
<article class="full">
	<img src="../img/doctors-kimball.jpg" alt="William E. Kimball, O.D." />
	<h1>William E. Kimball, O.D.</h1>
	<p class="jobtitle">Optometrist</p>
	<p>Dr. William Kimball received his BS degree from Wagner College in 1980 and went on to complete his optometric degree at the State University of New York, College of Optometry in 1984. He has continued his post-graduate training achieving both recognition as a Clinical Dipolmate in glaucoma and advanced practice certification. He has practiced in Connecticut since 1985 and founded Progressive Eye Care in 1989 along with his wife, Dr. Liann Kimball, and Dr. Richardson. Dr. Kimball practices at both our Southbury and Farmington locations.</p>
</article>
<article class="full">
	<img src="../img/doctors-richardson-1.jpg" alt="Victor A. Richardson, O.D." />
	<h1>Victor A. Richardson, O.D.</h1>
	<p class="jobtitle">Optometrist</p>
	<p>Dr. Richardson received his BA from Cornell University in 1979 and a Doctor of Optometry degree from the New York State College of Optometry in Manhattan in 1984. He completed his residency at the Northport Veteran's Administration Medical Center on Long Island in 1985. He started Progressive Eye Care with Dr. Kimball in 1989, and practices in both the Southbury and Farmington offices as well.</p>
</article>
<article class="full">
	<img src="../img/doctors-liann.jpg" alt="Liann Kimball, O.D." />
	<h1>Liann Kimball, O.D.</h1>
	<p class="jobtitle">Optometrist</p>
	<p>Dr. Liann Kimball obtained her BA in Biology from S.U.N.Y. Binghamton and then pursued her professional training at S.U.N.Y. College of Optometry where she received her Doctor of Optometry degree. She has continued her post graduate education achieving certification in Glaucoma and Advanced Practice Optometry. Dr. Kimball is a member of the Connecticut Optometric Association and the American Optometric Association. She practices at our Southbury location.</p>
</article>
<article class="full">
	<img src="../img/doctors-timothy.jpg" alt="Richard Timothy, L.O." />
	<h1>Richard Timothy, L.O.</h1>
	<p class="jobtitle">Optician</p>
	<p>Rick attended the University of Connecticut through 1983, and received his NABO and NCLE Certifications in 1993. He has been a practicing optician since 1991, acquiring his state licensure in 1996. Rick practices at our Southbury office, and has been here since 1999.</p>
</article>
<article class="full">
	<img src="../img/doctors-kris.jpg" alt="Kristen Clogston" />
	<h1>Kristen Clogston</h1>
	<p class="jobtitle">Optical Technician</p>
	<p>Kristen Clogston has been an Optical Technician for over 26 years.  She has been in our Farmington office since 1996.
	</p>
</article>
</section>
<aside>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>
</div>
</section> 
<?php echo $this->render('gui/footer.html',$this->mime,get_defined_vars()); ?>	

</body>
</html>