<section>
<article class="full">
<h1>Hyperopia</h1>
<p>Hyperopia, or farsightedness, is a common refractive error of the eye that makes it difficult to focus on near objects. Distance vision may or may not be affected, depending on the degree of hyperopia present.</p>
<p>Approximately 25 percent of the U.S. population is farsighted, according to several studies. Hyperopia frequently is confused with presbyopia, which is an age-related eye change that affects near vision. Farsightedness, on the other hand, typically occurs early in life.</p>
<p><b>What causes hyperopia?</b></p>
<p>Hyperopia is caused by the eyeball being too short, the cornea (the clear front surface of the eye) or the lens inside the eye being too flat, or a combination of these factors.</p>
<p>The result is that the optical elements of the farsighted eye are not powerful enough to bring light to a clear focus on the retina. Light is focused behind the retina, rather than directly on it.</p>
<p>A person with mild hyperopia sometimes can maintain clear reading vision without glasses by using additional focusing effort. But often this leads to headaches, eye strain and blurred vision from focusing fatigue.</p>
<p>Some children are born with hyperopia and "outgrow" it as the eyeball lengthens with normal growth. Other farsighted individuals remain farsighted throughout life.</p>
<p><b>Signs and symptoms of hyperopia</b></p>
<p>Signs and symptoms of uncorrected farsightedness typically are more severe when a farsighted person is reading, using a computer or doing other near work.</p>
<p>These signs and symptoms include:</p>
<p>Squinting</p>
<p>Eye strain</p>
<p>Tired eyes or general fatigue</p>
<p>Headache</p>
<p>Fluctuating or blurred vision</p>
<p>If you get these symptoms while wearing your glasses or contact lenses, you may need an eye exam and a new prescription.</p>
<p><b>Hyperopia treatment</b></p>
<p>Eyeglasses and contact lenses can correct farsightedness by converging light rays so they come to a proper focus on the retina. Lenses that correct hyperopia are thicker in the center than at the edge and are called "plus" lenses.</p>
<p>If the lens power designated on your glasses or contact lens prescription begins with a plus sign, like +2.50, you are farsighted.</p>
<p>Depending on the amount of farsightedness you have, you may need to wear glasses or contacts all the time, or you might only need glasses part-time for reading, working on a computer or doing other close-up work.</p>
<p>Refractive surgery, such as LASIK or PRK, is another option for correcting hyperopia.</p>
<p><i>Source: Hyperopia (Farsightedness) by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p class="credits">Article &#169;2011 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
    <li><a href="/articles/conditions-amblyopia">Amblyopia (Lazy Eye)</a></li>
    <li><a href="/articles/conditions-astigmatism">Astigmatism</a></li>
    <li><a href="/articles/conditions-blepharitis">Blepharitis</a></li>
    <li><a href="/articles/conditions-cataracts">Cataracts</a></li>
    <li><a href="/articles/conditions-cvs">Computer Vision Syndrome</a></li>
    <li><a href="/articles/conditions-diabetic">Diabetic Retinopathy</a></li>
    <li><a href="/articles/conditions-dry-eyes">Dry Eye Syndrome</a></li>
    <li><a href="/articles/conditions-allergies">Eye Allergies</a></li>
    <li><a href="/articles/conditions-floaters">Floaters and Spots</a></li>
    <li><a href="/articles/conditions-glaucoma">Glaucoma</a></li>
    <li><a href="/articles/conditions-keratoconus">Keratoconus</a></li>
    <li><a href="/articles/conditions-amd">Macular Degeneration</a></li>
    <li><a href="/articles/conditions-myopia">Myopia</a></li>
    <li><a href="/articles/conditions-ocular-hypertension">Ocular Hypertension</a></li>
    <li><a href="/articles/conditions-pinkeye">Pink Eye (Conjunctivitis)</a></li>
    <li><a href="/articles/conditions-presbyopia">Presbyopia</a></li>
    <li><a href="/articles/conditions-styes" class="last">Stye</a></li>
</ul> 
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>