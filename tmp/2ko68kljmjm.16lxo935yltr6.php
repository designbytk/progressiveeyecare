<section>
<article class="full">
<h1>LASIK Risks and Complications</h1>
<p>Since its introduction in the United States in 1998, many studies have shown LASIK eye surgery is safe and effective. An analysis of more than 3,000 scientific and clinical articles about LASIK published worldwide between 1998 and 2008 that was performed by the American Society of Cataract and Refractive Surgery (ASCRS) found that 95.4 percent of LASIK patients were satisfied with the outcome of their procedure. </p>
<p>Most LASIK complications can be resolved through laser re-treatment or medical treatment. Still, it's wise to consider the risks of and potential complications of refractive surgery and how to minimize them.</p>
<p>Selecting the right eye surgeon probably is the single most important step you can take to decrease risks associated with LASIK. An experienced, reputable surgeon will make sure you are a good candidate for LASIK before recommending refractive surgery. And if problems develop during or after the procedure, the surgeon should work closely with you to resolve them.</p>
<p><b>How common are LASIK complications?</b></p>
<p>Public confidence in LASIK vision correction surgery has grown in recent years because of high success rates involving millions of LASIK procedures performed in the United States and worldwide. With increasingly sophisticated technology used for the procedure, LASIK outcomes are better today than ever.</p>
<p>The U.S. military also has adopted the use of refractive surgery including LASIK to decrease reliance of troops on prescription eyewear. In a study of more than 16,000 U.S. Army personnel who underwent refractive surgery from 2000 through 2003, 86% achieved 20/20 or better uncorrected vision and 98% achieved 20/40 or better (the legal requirement for driving without glasses in most states).</p>
<p><b>LASIK complication rates</b></p>
<p>Many experienced LASIK surgeons say serious complication rates can be held well below one percent if surgical candidates are carefully screened prior to surgery. You may be eliminated as a candidate, for example, if you are pregnant or have certain conditions such as diabetes that may affect how well your eyes heal after LASIK. Be sure to discuss any health conditions you have with your eye doctor during your LASIK consultation or pre-operative exam.</p>
<p>An unusually large pupil size can be a risk factor for glare after LASIK, because in dark conditions large pupils might dilate larger than the diameter of the LASIK treatment zone on the front surface of the eye (cornea). Again, make sure you discuss any concerns you have about this or other matters with your eye surgeon before deciding to have LASIK or other laser vision correction surgery.</p>
<p><b>Other LASIK risk considerations</b></p>
<p>While the great majority of LASIK outcomes are favorable, some people experience serious and ongoing vision problems after LASIK. No surgical procedure is completely risk-free.</p>
<p>Some LASIK patients with excellent vision based on eye chart testing still can have bothersome side effects. For example, it’s possible a patient may see 20/20 or better without glasses after LASIK but still experience glare or halos around lights at night.</p>
<p><b>Common LASIK complications</b></p>
<p>LASIK complications can be caused by a number of factors. The most common complication following LASIK surgery is dry eye.</p>
<p>A study published in the March 2006 issue of <i>American Journal of Ophthalmology</i> found that 36 percent of LASIK patients experienced dry eye symptoms that lasted longer than six months after surgery.</p>
<p>In most cases, post-LASIK dry eye can be successfully treated with prolonged use of artificial tears or use of medicated eye drops to help stimulate natural tear production.</p>
<p>Careful screening of patients for pre-existing dry eye prior to surgery can eliminate many cases of dry eye after LASIK.Often, successful treatment of dry eye prior to LASIK can eliminate or reduce the severity of dry eye symptoms after surgery. </p>
<p>LASIK complications also can be associated with creation of the flap in the cornea, which is the first step of the LASIK procedure. This flap is lifted prior to re-shaping the underlying cornea with an excimer laser, and then is replaced to form a natural bandage.</p>
<p>Flap complications — such as partial flaps, free caps (flaps missing a hinge to keep it attached to the eye), flap wrinkles and displaced flaps — occur infrequently and usually do not cause permanent vision loss.</p>
<p>One large study published in a supplement to the September/October 2005 issue of Journal of Refractive Surgery found the incidence of LASIK flap complications to be 0.24 percent.Another study published in the April 2006 issue of <i>American Journal of Ophthalmology</i> found that flap complications occurred in 0.3 to 5.7 percent of LASIK procedures.</p>
<p>In most cases, flap complications are resolved by lifting and repositioning the flap.Sometimes, a bandage contact lens may be applied over the flap for a few days to help the flap stay properly positioned while it heals and adheres to the underlying cornea.</p>
<p>Use of a femtosecond laser rather than a mechanical surgical tool (called a microkeratome) appears to reduce the risk of some flap complications.</p>
<p>Other LASIK complications include:</p>
<p><b>Significant Undercorrection, Overcorrection or Regression:</b> An overcorrection or undercorrection of your refractive error (nearsightedness, farsightedness and/or astigmatism) can cause your vision to remain noticeably blurred after LASIK. Also, sometimes your vision can be corrected perfectly by LASIK surgery, but then some of your refractive error returns (this is called regression).</p>
<p>Usually, undercorrection, overcorrection and regression can be successfully treated with an additional laser treatment (called an enhancement). Enhancement surgeries typically are performed at least three months after the initial LASIK procedure to make sure your vision is stable. A study published in the April 2003 issue of Ophthalmology found that nearly 18 percent of LASIK patients (12 percent of treated eyes) needed an enhancement procedure.</p>
<p><b>Diffuse Lamellar Keratitis:</b> This condition (also called DLK), is inflammation under the LASIK flap associated with the accumulation of white blood cells. One study published in the <i>Journal of Refractive Surgery</i> in 2005, found the incidence of diffuse lamellar keratitis after LASIK surgery to be 2.3 percent. DLK usually can be successfully treated with steroid eye drops, but sometimes the eye surgeon might have to lift the flap and manually remove the accumulated inflammatory cells.</p>
<p><b>Eye Infection or Irritation:</b> Eye infections can occur after LASIK, but they are rare. To reduce the risk of post-LASIK eye infections, your eye doctor will prescribe antibiotic eye drops for you to use for several days after surgery.</p>
<p><b>Vision changes unrelated to LASIK</b></p>
<p>If you have LASIK in your 20s or 30s, be aware that your reading vision will change as you get older. This has nothing to do with your LASIK surgery – it’s caused by a normal age-related loss of focusing ability called presbyopia.</p>
<p>Because of presbyopia, most LASIK patients (like anyone else who sees well without glasses in younger years) will need reading glasses in middle age.</p>
<p><i>Source: LASIK Risks and Complications by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p class="credits">Article &#169;2011 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
	<ul>
    <li><a href="/articles/visionsurgery-basics">Corrective Eye Surgery Basics</a></li>
    <li><a href="/articles/visionsurgery-lasik">LASIK</a></li>
    <li><a href="/articles/visionsurgery-lasik-criteria">LASIK - Criteria for Success</a></li>
    <li><a href="/articles/visionsurgery-prk" class="last">PRK: The Original Laser Eye Surgery</a></li>
</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>