<section>
<article class="full">
<h1>Corrective Eye Surgery Basics</h1>
<p>Are you tired of wearing glasses or contact lenses? Today, there are several types of vision correction surgery that can improve your eyesight and help you see clearly with prescription eyewear.</p>
<p>LASIK is the most popular vision-correcting or “refractive” surgery and has been performed in the United States since 1998.</p>
<p>LASIK (laser-assisted in situ keratomileusis) is a two-step procedure: First, a thin flap is created on the clear front surface of the eye (cornea) with a femtosecond laser or a bladed surgical instrument called a microkeratome. This flap is folded back and the underlying cornea tissue is reshaped with an excimer laser.</p>
<p>After the laser treatment, the flap is repositioned and heals without the need for stitches. The entire surgery takes only a few minutes per eye.</p>
<p>One of the primary reasons for the popularity of LASIK is that there is little or no discomfort after the procedure, and vision usually is quite clear within minutes.</p>
<p>But there are other refractive surgery options to correct your vision if you are not a good candidate for LASIK or if you and your eye doctor feel a different procedure better suits your needs. Here are a few alternatives:</p>
<p><b>PRK</b></p>
<p>PRK (photorefractive keratectomy) was the first laser vision correction procedure approved in the United States, receiving FDA approval in 1995. Because of its superior predictability, PRK soon replaced radial keratotomy (RK), which was the only viable surgical treatment for nearsightedness available at the time. PRK reduced many of the complications of RK, including fluctuating vision, glare, halos around lights, infection, unpredictable outcomes, decreased visual acuity and regression (return of nearsightedness). </p>
<p>Like LASIK, PRK uses an excimer laser to reshape the cornea and correct vision. But with PRK, the laser treatment is applied directly to the surface of the eye, rather than under a flap of corneal tissue.</p>
<p>Visual outcomes of PRK are comparable to those of LASIK and other laser vision correction procedures. But recovery time after PRK is longer than after LASIK. Since no corneal flap is created in PRK, the thin outer protective layer of the cornea (the epithelium) must grow back after surgery, which can take several days. Until this process is complete, there is some discomfort after PRK for a few days and vision can be quite blurred for a week or two until the eye heals.</p>
<p>The number of PRK procedures declined sharply when LASIK was introduced, because there is usually little or no discomfort after LASIK and vision recovers faster. However, PRK has made a comeback in recent years due to more effective pain management techniques and because it poses less risk of certain complications.</p>
<p><b>LASEK</b></p>
<p>LASEK (laser-assisted sub-epithelial keratomileusis) is a modification of LASIK in which the corneal flap is thinner, containing only epithelial cells. The delicate epithelium is removed by loosening it from the underlying cornea with an alcohol solution. It’s then pushed to the side and the laser treatment is applied. The epithelial “flap” is then gently replaced and covered with a bandage contact lens until it reattaches to the underlying cornea.</p>
<p>In many cases, there is less post-operative discomfort with LASEK compared with PRK, and vision recovery may be faster. LASEK sometimes is preferred over LASIK in cases when the patient’s cornea is considered too thin for a safe LASIK procedure.</p>
<p><b>Epi-LASIK</b></p>
<p>Epi-LASIK is much like LASEK, except a special cutting tool is used to separate the epithelium from the underlying cornea prior to the laser treatment. This eliminates the possibility of an adverse reaction to alcohol placed on the eye and may speed healing after surgery, compared with LASEK. Like LASEK, epi-LASIK sometimes is preferred over LASIK if there are concerns about corneal thickness. 
</p>
<p><b>IntraLASIK, iLASIK</b></p>
<p>IntraLASIK and iLASIK are terms sometimes used to describe a LASIK procedure when the corneal flap is created with a femtosecond laser instead of a microkeratome for blade-free, all-laser surgery. All-laser LASIK eliminates the risk of certain complications that can occur when the flap is created with a microkeratome. 
</p>
<p><b>Wavefront LASIK</b></p>
<p>Wavefront LASIK (also called wavefront-guided LASIK or custom LASIK) is an advanced type of LASIK where the laser treatment is determined by a computerized mapping of the power of the eye called wavefront analysis. Wavefront-guided procedures are more precise than ablations determined by using only an eyeglasses prescription, and they can correct subtle optical imperfections called “higher-order aberrations” (HOA) that regular (non-wavefront) LASIK can’t treat. Several studies show wavefront-guided LASIK produces sharper vision than conventional LASIK and may reduce the risk of post-LASIK glare and halos.</p>
<p>Another advanced type of LASIK is wavefront-optimized LASIK, which is designed to reduce a specific type of HOA called spherical aberration. Some studies have found that wavefront-optimized LASIK produces visual outcomes that are equal to wavefront-guided LASIK.</p>
<p>Wavefront technology (both wavefront-guided and wavefront-optimized) can also be used for PRK and other laser vision correction procedures.</p>
<p><b>Conductive keratoplasty</b></p>
<p>Conductive keratoplasty (CK) is a non-laser refractive surgery that uses a hand-held instrument to deliver low-heat radio waves to a number of spots in the peripheral cornea. This causes the corneal tissue to shrink in these areas, which increases the curvature of the cornea, thereby correcting mild amounts of farsightedness or restoring usable near vision to people over age 40 who have presbyopia.</p>
<p>CK for presbyopia is called NearVision CK, and it can be used to correct presbyopia for people who previously had LASIK surgery.</p>
<p><b>Phakic IOLs</b></p>
<p>Phakic IOLs (intraocular lenses) are small lenses implanted inside the eye to correct vision problems. The lenses can be place in front of the pupil (attached to the front of the iris) or behind it. “Phakic” refers to the fact that the eye’s natural lens remains in the eye during the procedure.</p>
<p>Phakic IOL implantation can correct higher amounts of nearsightedness than LASIK. But because it’s an internal eye procedure, there are more risks associated with phakic IOL surgery than with LASIK. Cost of the procedure also is significantly higher.</p>
<p><b>Refractive Lens Exchange</b></p>
<p>Refractive Lens Exchange (RLE) is another non-laser, internal eye procedure. RLE is much like cataract surgery, but instead of removing the eye's natural lens that has grown cloudy, the surgeon removes a clear natural lens and replaces it with an artificial lens of a different shape to reduce the need for glasses.</p>
<p>RLE has a higher risk of complications and is more expensive than LASIK. It usually is recommended only for cases of severe vision correction needs or for patients who are at risk of cataracts and already are presbyopic. In some cases, multifocal or accommodating IOLs (see below) are used in RLE to reduce the need for reading glasses after surgery.</p>
<p><b>Cataract Surgery</b></p>
<p>Even cataract surgery can be considered a refractive procedure. New intraocular lenses (IOLs) implanted during cataract surgery can partially restore a person's near vision in addition to correcting nearsightedness and farsightedness. These lenses — called multifocal IOLs or accommodating IOLs —currently are being used by many cataract surgeons and are producing excellent results.</p>
<p>While Medicare and health insurance usually will cover the basic costs of cataract surgery, choosing multifocal or accommodating IOLs that potentially can restore a full range of vision will increase your cataract surgery cost, and you will be responsible for paying the added cost of these premium lenses.</p>
<p><b>Which procedure is right for you?</b></p>
<p>If you are interested in LASIK or other vision correction surgery, your first step is to schedule a comprehensive eye exam and refractive surgery consultation. During this visit, your eye doctor will determine if you are a good candidate for refractive surgery and discuss which procedure might be best for your needs.</p>
<p>If you are a good candidate, your eye doctor can help you choose an experienced refractive surgeon in your area for your vision correction surgery.</p>
<p><i>Source: Corrective Eye Surgery Basics by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p class="credits">Article &#169;2011 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
	<ul>
    <li><a href="/articles/visionsurgery-lasik">LASIK</a></li>
    <li><a href="/articles/visionsurgery-lasik-criteria">LASIK - Criteria for Success</a></li>
    <li><a href="/articles/visionsurgery-risks">LASIK Risks and Complications</a></li>
    <li><a href="/articles/visionsurgery-prk" class="last">PRK: The Original Laser Eye Surgery</a></li>
</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>