
<!DOCTYPE html>
<html lang="en">
<?php echo $this->render('gui/head.html',$this->mime,get_defined_vars()); ?>	

<body>
<?php echo $this->render('gui/header.html',$this->mime,get_defined_vars()); ?>
<figure>
<div>
	<h2><i class="icon-chevron-sign-right"></i>ARTICLES</h2>
</div>
</figure>
	
<section id="content">
<div>
	<div class="split">
<article>	
	<h1>Children's Vision</h1>
	<img src="../img/article-children-hdr.jpg" alt="Mother and Daughter with Glasses" />
	<ul>
    <li><a href="/articles/children-faq">Children's Vision FAQs</a></li>
    <li><a href="/articles/children-learning">Learning-Related Vision Problems</a></li>
    <li><a href="/articles/children-infants" class="last">Your Infant's Visual Development</a></li>
</ul> 
</article>
<article>
	<h1>Vision Over 60</h1>
	<img src="../img/article-over60-hdr.jpg" alt="Man over 60" />
	<ul>
    <li><a href="/articles/over60-protect" class="last">Eight Ways To Protect Your Eyesight</a></li>
</ul> 
</article>
<article>
	<h1>Eyeglasses</h1>
	<img src="../img/article-eyeglasses-hdr.jpg" alt="Woman with Eyeglasses" />
	<ul>
	<li><a href="/articles/eyeglasses-basics">The Basics of Eyeglasses</a></li>
    <li><a href="/articles/eyeglasses-frames-materials">Eyeglass Frame Materials</a></li>
    <li><a href="/articles/eyeglasses-lenses">Lens Options for Eyeglasses</a></li>
    <li><a href="/articles/eyeglasses-specialty" class="last">Specialty Eyewear</a></li>
</ul> 
</article>
<article>
	<h1>Contact Lenses</h1>
	<img src="../img/article-contacts-hdr.jpg" alt="Finger with Contact Lens" />
	<ul>
    <li><a href="/articles/contacts-bifocal">Bifocal and Multifocal Contact Lenses</a></li>
    <li><a href="/articles/contacts-basics">Contact Lens Basics</a></li>
    <li><a href="/articles/contacts-hardtofit">Contact Lenses for the 'Hard-to-Fit' Patient</a></li>
    <li><a href="/articles/contacts-gp">Gas Permeable (GP) Contact Lenses</a></li>
    <li><a href="/articles/contacts-orthok">Orthokeratology</a></li>
</ul> 
</article>
<article>
	<h1>Eye Exams</h1>
	<img src="../img/services-home-hdr.jpg" alt="Glasses on Eyechart" />
	<ul>
   <li><a href="/articles/eyeexams-expectations" class="last">Your Comprehensive Eye Exam</a></li>
</ul> 
</article>
</div>
<div class="split">
<article>
	<h1>LASIK and Vision Surgery</h1>
	<img src="../img/article-lasik-hdr.jpg" alt="Pupil Close Up" />
	<ul>
    <li><a href="/articles/visionsurgery-basics">Corrective Eye Surgery Basics</a></li>
    <li><a href="/articles/visionsurgery-lasik">LASIK</a></li>
    <li><a href="/articles/visionsurgery-lasik-criteria">LASIK - Criteria for Success</a></li>
    <li><a href="/articles/visionsurgery-risks">LASIK Risks and Complications</a></li>
    <li><a href="/articles/visionsurgery-prk" class="last">PRK: The Original Laser Eye Surgery</a></li>
</ul> 
</article>
<article>
	<h1>Nutrition and Vision</h1>
	<img src="../img/article-low-hdr.jpg" alt="Eye Closed" />
	<ul>
    <li><a href="/articles/nutrition" class="last">Nutrition and Vision</a></li>
</ul> 
</article>
<article>
	<h1>Problems and Diseases</h1>
	<img src="../img/article-disease-hdr.jpg" alt="Stethescope" />
	<ul>
    <li><a href="/articles/conditions-amblyopia">Amblyopia (Lazy Eye)</a></li>
    <li><a href="/articles/conditions-astigmatism">Astigmatism</a></li>
    <li><a href="/articles/conditions-blepharitis">Blepharitis</a></li>
    <li><a href="/articles/conditions-cataracts">Cataracts</a></li>
    <li><a href="/articles/conditions-cvs">Computer Vision Syndrome</a></li>
    <li><a href="/articles/conditions-diabetic">Diabetic Retinopathy</a></li>
    <li><a href="/articles/conditions-dry-eyes">Dry Eye Syndrome</a></li>
    <li><a href="/articles/conditions-allergies">Eye Allergies</a></li>
    <li><a href="/articles/conditions-floaters">Floaters and Spots</a></li>
    <li><a href="/articles/conditions-glaucoma">Glaucoma</a></li>
    <li><a href="/articles/conditions-hyperopia">Hyperopia</a></li>
    <li><a href="/articles/conditions-keratoconus">Keratoconus</a></li>
    <li><a href="/articles/conditions-amd">Macular Degeneration</a></li>
    <li><a href="/articles/conditions-myopia">Myopia</a></li>
    <li><a href="/articles/conditions-ocular-hypertension">Ocular Hypertension</a></li>
    <li><a href="/articles/conditions-pinkeye">Pink Eye (Conjunctivitis)</a></li>
    <li><a href="/articles/conditions-presbyopia">Presbyopia</a></li>
    <li><a href="/articles/conditions-styes" class="last">Stye</a></li>
</ul> 
</article>
</div>
<aside>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>
</aside>
	</div>
</section> 
<?php echo $this->render('gui/footer.html',$this->mime,get_defined_vars()); ?>	

</body>
</html>
                                                      