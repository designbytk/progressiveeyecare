<section>
<article class="full">
<h1>Glaucoma</h1>
<p>Glaucoma refers to a group of related eye diseases that cause damage to the optic nerve that transmits visual information from the eye to the brain. Glaucoma usually, but not always, is associated with increased pressure inside the eye (called intraocular pressure, or IOP).</p>
<p>Glaucoma typically affects your peripheral vision first. There typically is little or no discomfort with the onset of glaucoma, and you can lose a great deal of your vision from the disease before you are aware anything is happening. If uncontrolled or left untreated, glaucoma can eventually lead to blindness.</p>
<p>Glaucoma currently is the second leading cause of blindness in the United States, with an estimated 2.5 million Americans being affected by the disease. Due to the aging of the U.S. population, it’s expected that more than 3 million Americans will have glaucoma by the year 2020.</p>
<p><b>Signs and symptoms of glaucoma</b></p>
<p>Glaucoma sometimes is called the "silent thief of sight," because most types typically cause no pain and produce no symptoms. For this reason, glaucoma often progresses undetected until the optic nerve already has been irreversibly damaged, with varying degrees of permanent vision loss.</p>
<p>But one type of glaucoma — acute angle-closure glaucoma — has a sudden onset and can be accompanied by symptoms of intense eye pain, nausea, and vomiting. If you have these symptoms, make sure you immediately see an eye care practitioner or visit the emergency room so steps can be taken to prevent permanent vision loss.</p>
<p><b>What causes glaucoma?</b></p>
<p>The cause of glaucoma generally is a failure of the eye to maintain an appropriate balance between the amount of fluid produced inside the eye and the amount that drains away. The reason for this imbalance varies, depending on the type of glaucoma you have.</p>
<p>The eye needs internal fluid pressure to retain its globe-like shape and ability to see. When glaucoma damages the ability of internal eye structures to properly regulate IOP, eye pressure can rise to dangerously high levels and cause permanent vision loss.</p>
<p><b>Types of glaucoma</b></p>
<p>The two major types of glaucoma are primary open-angle glaucoma (POAG) and acute angle-closure glaucoma. The “angle” refers to the structure inside the eye that is responsible for the normal drainage if intraocular fluid, which is located near the junction between the iris and the front surface of the eye near the periphery of the cornea.</p>
<p><b>Primary open-angle glaucoma (POAG):</b> About half of Americans with POAG (also called chronic glaucoma) don't know they have it. POAG gradually and painlessly reduces your peripheral vision. Often by the time you notice it, permanent vision loss already has occurred. If your IOP remains high, the destruction can progress until tunnel vision develops, and you will be able to see only objects that are straight ahead.</p>
<p><b>Acute angle-closure glaucoma:</b> Angle-closure or narrow-angle glaucoma produces sudden symptoms such as eye pain, headaches, halos around lights, dilated pupils, vision loss, red eyes, nausea and vomiting. These signs may last for a few hours, and then return again for another round. Each attack takes with it part of your field of vision.</p>
<p><b>Normal-tension glaucoma:</b> Like POAG, normal-tension glaucoma (also called normal-pressure glaucoma, low-tension glaucoma, or low-pressure glaucoma) is an open-angle type of glaucoma with little or no discomfort. In normal-tension glaucoma, the eye's IOP remains in the normal range and damage to the optic nerve may not be detected until significant loss of peripheral vision has occurred.</p>
<p>The cause of normal-tension glaucoma is unknown, but some experts believe it is related to poor blood flow to the optic nerve. Risk factors for normal-tension glaucoma include being of Japanese descent, female gender and a history of vascular disease.</p>
<p><b>Congenital glaucoma:</b> This inherited form of glaucoma is present at birth, with 80 percent of cases diagnosed in the first year of life. Infants with congenital glaucoma are born with narrow angles or some other defect in the fluid drainage system of the eye. Symptoms include a cloudy, hazy or protruding eye. Congenital glaucoma typically occurs more in boys than in girls.</p>
<p><b>Pigmentary glaucoma:</b> This rare form of glaucoma is caused by pigment from the iris floating freely in the anterior chamber of the eye and eventually clogging the drainage angle, preventing intraocular fluid from leaving the eye. Over time, an inflammatory response to the blocked angle damages the drainage system. Typically there are no early symptoms of pigmentary glaucoma, though some pain and blurry vision may occur after exercise or other physical exertion. This type of glaucoma most frequently affects white males in their mid-30s to mid-40s.</p>
<p><b>Secondary glaucoma:</b> This is the term used to describe chronic glaucoma that develops after an eye injury, infection or inflammation, or is caused by some other abnormality such as a tumor in the eye or an enlarged cataract.</p>
<p><b>How is glaucoma detected?</b></p>
<p>Routine eye exams are required for the diagnosis and management of glaucoma.</p>
<p>The "glaucoma test" during an eye exam actually is just a simple procedure called tonometry that measures your intraocular pressure. Two common methods to measure IOP are Goldmann applanation tonometry (GAT) and a non-contact tonometry (NCT).</p>
<p>For GAT, numbing eye drops are used and a small probe gently rests against your eye's surface. Because iof its direct contact with the eye, Goldmann applanation tonometry generally is considered the "gold standard" for IOP measurement.</p>
<p>With non-contact tonometry, nothing touches your eye but a puff of air. Many studies have shown NCT measurements are comparable to GAT measurements, without the need for numbing eye drops and touching the eye's surface.</p>
<p>An abnormally high IOP reading indicates a problem with the amount of fluid inside the eye. Either the eye is producing too much fluid, or it's not draining properly.</p>
<p>Additional tests used for the diagnosis and management of glaucoma include retinal photography other imaging techniques such as optical coherence tomography (OCT) to monitor the health and stability of the head of the optic nerve that is visible inside the eye.</p>
<p>Visual field testing also is essential to monitor whether blind spots are developing in your field of vision from glaucoma damage to the optic nerve. Visual field testing involves staring straight ahead into a machine and clicking a button when you notice a blinking light in your peripheral vision. This testing typically is repeated at regular intervals so your eye doctor can determine if there is progressive vision loss from glaucoma.</p>
<p>Your eye doctor may also visually inspect the drainage angle of the eye using special lenses that enable him or her to see the angle from different vantage points. This is called gonioscopy.</p>
<p><b>Glaucoma treatments</b></p>
<p>Depending on the severity of the disease, treatment for glaucoma can involve the use of topical and oral medicine, conventional (bladed) surgery, laser surgery or a combination of these treatments. Medicated eye drops aimed at lowering IOP usually are tried first to control glaucoma.</p>
<p>Since there typically is no eye pain associated with glaucoma, people sometimes become careless about using their glaucoma medicines as directed by their eye doctor. In fact, non-compliance with a program of prescribed glaucoma medication is a major reason for blindness resulting from glaucoma.</p>
<p>If you find that the eye drops you are using for glaucoma are uncomfortable or inconvenient, never discontinue them without first consulting your eye doctor about a possible alternative therapy.</p>
<p>Glaucoma surgery procedures (whether laser or non-laser) are designed to decrease the production of intraocular fluid or increase the outflow (drainage) of this same fluid. Occasionally, a procedure will accomplish both.</p>
<p>Currently the goal of glaucoma surgery and other glaucoma therapy is to reduce or stabilize intraocular pressure (IOP). When this goal is accomplished, progressive damage to the optic nerve and vision loss often can be prevented or halted.</p>
<p><b>Early detection is key</b></p>
<p>Early diagnosis and treatment is the best way to prevent vision loss from glaucoma. See your eye doctor routinely for comprehensive eye exams that include a check of your IOP.</p>
<p>People at high risk for glaucoma due to elevated intraocular pressure, a family history of glaucoma, advanced age or an unusual optic nerve appearance may need more frequent exams.</p>
<p><i>Source: Glaucoma: Types, Symptoms, Diagnosis and Treatment by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p class="credits">Article &#169;2011 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.</p>
<p></p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
    <li><a href="/articles/conditions-amblyopia">Amblyopia (Lazy Eye)</a></li>
    <li><a href="/articles/conditions-astigmatism">Astigmatism</a></li>
    <li><a href="/articles/conditions-blepharitis">Blepharitis</a></li>
    <li><a href="/articles/conditions-cataracts">Cataracts</a></li>
    <li><a href="/articles/conditions-cvs">Computer Vision Syndrome</a></li>
    <li><a href="/articles/conditions-diabetic">Diabetic Retinopathy</a></li>
    <li><a href="/articles/conditions-dry-eyes">Dry Eye Syndrome</a></li>
    <li><a href="/articles/conditions-allergies">Eye Allergies</a></li>
    <li><a href="/articles/conditions-floaters">Floaters and Spots</a></li>
    <li><a href="/articles/conditions-hyperopia">Hyperopia</a></li>
    <li><a href="/articles/conditions-keratoconus">Keratoconus</a></li>
    <li><a href="/articles/conditions-amd">Macular Degeneration</a></li>
    <li><a href="/articles/conditions-myopia">Myopia</a></li>
    <li><a href="/articles/conditions-ocular-hypertension">Ocular Hypertension</a></li>
    <li><a href="/articles/conditions-pinkeye">Pink Eye (Conjunctivitis)</a></li>
    <li><a href="/articles/conditions-presbyopia">Presbyopia</a></li>
    <li><a href="/articles/conditions-styes" class="last">Stye</a></li>
</ul> 
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>