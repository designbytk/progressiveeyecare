<section>
<article class="full">
<h1>Diabetic Retinopathy</h1>
<p>Diabetic retinopathy is damage to the light-sensitive retina at the back of the eye due to Type 1 or Type 2 diabetes. It is the leading cause of blindness among Americans under the age of 65.</p>
<p>Currently more than 5 million Americans age 40 and older have diabetic retinopathy, and that number will grow to about 16 million by 2050, according to the U.S. Centers for Disease Control (CDC) and other researchers.</p>
<p>Complications of diabetic retinopathy include retinal detachment and glaucoma.</p>
<p><b>Signs and symptoms of diabetic retinopathy</b></p>
<p>Signs and symptoms of diabetic retinopathy include:</p>
<p>Fluctuating, blurry and/or distorted vision</p>
<p>Eye floaters and spots</p>
<p>Development of shadows or blind spots in your field of view</p>
<p>Double vision</p>
<p>Near vision problems unrelated to presbyopia</p>
<p>If you experience any of these symptoms, see your eye doctor immediately. If you are diabetic, you should see your eye doctor at least once a year for a dilated eye exam, even if you have no visual symptoms.</p>
<p>If your eye doctor suspects diabetic retinopathy, a special test called fluorescein angiography may be performed. In this test, dye is injected into the body and then gradually appears within the retina due to blood flow. Your eyecare practitioner will photograph the retina as the dye passes through the blood vessels in the retina.</p>
<p>Evaluating these pictures tells your doctor or a retina specialist if signs of diabetic retinopathy exist, and how far the disease has progressed.</p>
<p><b>What causes diabetic retinopathy?</b></p>
<p>Diabetes damages delicate blood vessels in the retina, causing them to bleed or leak fluid. It also can cause swelling of the retina, leading to blurred and distorted vision.</p>
<p>These early changes are called non-proliferative diabetic retinopathy (NPDR).</p>
<p><span class="s1">In the advanced stage of the disease</span>, called proliferative diabetic retinopathy (PDR), new blood vessels grow on the surface of the retina. These abnormal blood vessels can lead to serious vision problems because they can break and bleed into the interior of the eye. PDR is much more serious than non-proliferative diabetic retinopathy and can lead to blindness.</p>
<p>The risk of both forms of diabetic retinopathy increase the longer you have diabetes. According to the American Academy of Ophthalmology, all diabetics who have the disease long enough eventually will develop at least some degree of diabetic retinopathy, though less advanced forms of the eye disease may not lead to vision loss.</p>
<p>As soon as you've been diagnosed with diabetes, you need to have a dilated eye exam at least once a year.</p>
<p><b>How is diabetic retinopathy treated?</b></p>
<p>In most cases, significant vision loss from diabetic retinopathy can be avoided if treated in time.</p>
<p>Diabetic retinopathy can be treated with a laser to seal off leaking blood vessels and inhibit the growth of new vessels. Called laser photocoagulation, this treatment is painless and takes only a few minutes.</p>
<p>In severe cases of diabetic retinopathy where blood has leaked into the interior of the eye and is obscuring vision, a surgical procedure called a vitrectomy may be performed to remove the blood from the eye.</p>
<p>Recent research shows that certain medications injected directly into the eye may be able to slow or prevent vision loss from diabetic retinopathy. These medicines, called anti-VEGF drugs, are designed to stop or reduce the formation of abnormal retinal blood vessels and prevent or reduce swelling of the retina.</p>
<p><b>Prevention of diabetic retinopathy</b></p>
<p>You can significantly reduce your risk of diabetic retinopathy by using common sense and taking good care of yourself:</p>
<p>Maintain a healthy diet.</p>
<p>Exercise regularly.</p>
<p>Have routine physical exams and eye exams.</p>
<p>If you have diabetes, monitor your blood sugar regularly and keep it under control.</p>
<p>Following these simple guidelines can prevent many cases of Type 2 diabetes and diabetic retinopathy.</p>
<p><i>Source:  </i><span class="s2"><i>Diabetic Retinopathy by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p>Article ©2011 Access Media Group LLC.All rights reserved.Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
    <li><a href="/articles/conditions-amblyopia">Amblyopia (Lazy Eye)</a></li>
    <li><a href="/articles/conditions-astigmatism">Astigmatism</a></li>
    <li><a href="/articles/conditions-blepharitis">Blepharitis</a></li>
    <li><a href="/articles/conditions-cataracts">Cataracts</a></li>
    <li><a href="/articles/conditions-cvs">Computer Vision Syndrome</a></li>
    <li><a href="/articles/conditions-dry-eyes">Dry Eye Syndrome</a></li>
    <li><a href="/articles/conditions-allergies">Eye Allergies</a></li>
    <li><a href="/articles/conditions-floaters">Floaters and Spots</a></li>
    <li><a href="/articles/conditions-glaucoma">Glaucoma</a></li>
    <li><a href="/articles/conditions-hyperopia">Hyperopia</a></li>
    <li><a href="/articles/conditions-keratoconus">Keratoconus</a></li>
    <li><a href="/articles/conditions-amd">Macular Degeneration</a></li>
    <li><a href="/articles/conditions-myopia">Myopia</a></li>
    <li><a href="/articles/conditions-ocular-hypertension">Ocular Hypertension</a></li>
    <li><a href="/articles/conditions-pinkeye">Pink Eye (Conjunctivitis)</a></li>
    <li><a href="/articles/conditions-presbyopia">Presbyopia</a></li>
    <li><a href="/articles/conditions-styes" class="last">Stye</a></li>
</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>
