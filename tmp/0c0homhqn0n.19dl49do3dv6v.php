<head>
	<meta charset="utf-8">
	<title>Progressive Eye Care | Farmington | Southbury | Connecticut</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="stylesheet" href="<?php echo $site; ?>/css/main.css" type="text/css" />
	<!-- google fonts -->
	<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,300|Syncopate' rel='stylesheet' type='text/css'>
	<?php if ($currentpage=='home'): ?>
	
	<link rel="stylesheet" href="<?php echo $site; ?>/css/fractionslider.css" type="text/css" />
	
	<?php endif; ?>
	<!-- Font Awesome Icons	-->
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	
<!--	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>-->
	<script type="text/javascript" src="<?php echo $site; ?>/js/jquery-1.9.0.min.js"></script> <!--jQuery local -->
	<script type="text/javascript" src="<?php echo $site; ?>/js/jquery.leanModal.min.js"></script> <!--Image Zoom - Fancy Zoom -->
	
	<script src="<?php echo $site; ?>/js/modernizr.custom.43202.js"></script>
	<!-- SLIDER	-->
	<?php if ($currentpage=='home'): ?>
	 
	<script src="<?php echo $site; ?>/js/jquery.fractionslider.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo $site; ?>/js/fractionslider.js" type="text/javascript" charset="utf-8"></script>
	
	<?php endif; ?>
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- typekit -->
	<script type="text/javascript" src="//use.typekit.net/mkr6xqk.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$("#formdl").leanModal({ closeButton: ".modal_close" });
			//$('#formdl').fancyZoom({scaleImg: false, closeOnClick: true, width:200, height:100});
		});
	</script>


<?php if ($currentpage=='contact'): ?>
	
	<!-- contact validation -->
	<script type="text/javascript" src="<?php echo $site; ?>/js/jquery.form.js"></script>
	<!-- google map -->
	<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
	<script>
		//Farmington
		var farmington=new google.maps.LatLng(41.737728, -72.795779);
		
		var marker_f=new google.maps.Marker({
		    position:farmington,
		    url: 'https://maps.google.com/maps?daddr=230+Farmington+Ave,+Farmington,+CT+06032',
		    title: 'Get Directions',
		    animation:google.maps.Animation.DROP
		});
		
		//Southbury
		var southbury=new google.maps.LatLng(41.493493,-73.166838);
		
		var marker_s=new google.maps.Marker({
		    position:southbury,
		    url: 'https://maps.google.com/maps?daddr=1449+Old+Waterbury+Rd,+Southbury,+CT',
		    title: 'Get Directions',
		    animation:google.maps.Animation.DROP
		});
		
		function initialize()
		{
		//Farmington
		var mapProp_f = {
		    center:farmington,
		    zoom: 17,
		    draggable: false,
		    scrollwheel: false,
		    mapTypeId:google.maps.MapTypeId.ROADMAP
		};
		
		var map_f=new google.maps.Map(document.getElementById("map_farmington"),mapProp_f);
		
		marker_f.setMap(map_f);
		
		//Southbury
		var mapProp_s = {
		    center:southbury,
		    zoom: 17,
		    draggable: false,
		    scrollwheel: false,
		    mapTypeId:google.maps.MapTypeId.ROADMAP
		};
		
		var map_s=new google.maps.Map(document.getElementById("map_southbury"),mapProp_s);
		
		marker_s.setMap(map_s);
		}
		
		google.maps.event.addDomListener(window, 'load', initialize);
		google.maps.event.addListener(marker_f, 'click', function() {window.open(marker_s.url);});
		google.maps.event.addListener(marker_s, 'click', function() {window.open(marker_s.url);});
	
		
		
		
		
	</script> 
	
	<?php endif; ?>
	
	
</head>