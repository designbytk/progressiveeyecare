<section>
<article class="full">
<h1>Dry Eye Syndrome</h1>
<p>Dry eye syndrome (or “dry eye”) is the chronic lack of sufficient lubrication and moisture on the surface of the eye, causing discomfort, contact lens intolerance and increased risk of eye infections.</p>
<p>Dry eye is common, with some studies reporting that 10 to 20 million Americans experience some degree of dry eye syndrome.</p>
<p>Common risk factors for dry eye include increasing age, a dry environment and use of certain medications.</p>
<p><b>Signs and symptoms of dry eye</b></p>
<p>Signs and symptoms of dry eye syndrome include:</p>
<p>Red, irritated eyes</p>
<p>A burning or scratchy sensation</p>
<p>Feeling something is "in" your eye (called a foreign body sensation)</p>
<p>Fluctuating or blurred vision</p>
<p>Eye pain</p>
<p>Contact lens discomfort</p>
<p>It may seem odd, but another symptom of dry eye syndrome is a watery eye. This is because, as a reaction to dry eye irritation, the tear glands sometimes secrete very watery tears as a protective mechanism to prevent eye damage from a dry eye condition.</p>
<p><b>What causes dry eyes?</b></p>
<p>Dry eye is caused by the tear glands failing to secrete an adequate amount of tears or producing a tear film that, because of insufficient oiliness, evaporates too quickly.</p>
<p>These problems can be due to aging or a side effect of many medications, such as antihistamines, antidepressants, certain blood pressure medicines and birth control pills.</p>
<p>Dry eye can also be caused by chronic exposure to a dry, dusty or windy climate with low humidity. Air conditioning and forced air heating systems at home and at work also can dry out your eyes.</p>
<p>Another cause is failing to blink your eyes normally to remoisten them. This frequently occurs during computer work.</p>
<p>Dry eye syndrome also is associated with certain systemic diseases such as lupus, rheumatoid arthritis, rosacea and Sjogren's syndrome.</p>
<p>Long-term contact lens wear, incomplete closure of the eyelids, eyelid disease and a deficiency of the tear-producing glands are other causes.</p>
<p>Dry eye syndrome is more common in women, possibly due to hormone fluctuations. Recent research suggests that smoking, too, can increase your risk of dry eye syndrome.</p>
<p>Dry eye also has been associated with incomplete lid closure following blepharoplasty — a popular cosmetic surgery to eliminate droopy eyelids.</p>
<p><b>Treatment for dry eye</b></p>
<p>There are several treatments for dry eye, based on the severity of the condition.</p>
<p>For mild dry eye, your eye doctor might recommend artificial tears, which are lubricating eye drops that are designed to alleviate the dry, scratching feeling and foreign body sensation of dry eye. Prescription eye drops for dry eye go one step further: they help increase your tear production.</p>
<p>If you wear contact lenses, be aware that some artificial tears and lubricating eye drops cannot be used during contact lens wear. You may need to remove your lenses before using the drops and wait 15 minutes or longer (check the label) before reinserting them.</p>
<p><span class="s1">Use only the brand of artificial tears your eye doctor recommends. </span>Avoid self-medicating a dry eye condition by choosing artificial tears randomly in a drug store. Some products might actually make your symptoms worse.</p>
<p>To reduce the effects of sun, wind and dust on dry eyes, wear sunglasses when outdoors. Close-fitting wraparound styles offer the best protection.</p>
<p>Indoors, an air cleaner can filter out dust and other particles from the air, while a humidifier adds moisture to air that's too dry because of air conditioning or heating.</p>
<p>For more significant cases of dry eye, your eye doctor might recommend punctal plugs. These tiny devices are inserted in the tear drainage ducts in your eyelids to slow the drainage of tears away from your eyes, thereby keeping your eyes more moist.</p>
<p>Doctors sometimes recommend special nutritional supplements such as flaxseed oil or fish oil to decrease dry eye symptoms. Drinking more water also may relieve dryness symptoms.</p>
<p>If medications are the cause of dry eyes, switching to a different medication or a different medical treatment may resolve the problem. However, always consult with your doctor before switching or discontinuing any medication.</p>
<p>Treating any underlying eyelid disease, such as blepharitis, helps as well. This may call for antibiotic or steroid drops, plus frequent eyelid scrubs with an antibacterial shampoo.</p>
<p>If you are considering LASIK, be aware that dry eyes may disqualify you for the surgery, at least until your dry eye condition is successfully treated. Dry eyes increase your risk for poor healing after LASIK, so most surgeons will want to treat the dry eyes first, to ensure a good LASIK outcome. This goes for other types of vision correction surgery, as well.</p>
<p>Also, be aware that dry eye is a common side effect of LASIK eye surgery, especially if you have any signs or symptoms of dry eyes prior to surgery.</p>
<p><i>Source:  </i><span class="s2"><i>Dry Eye Syndrome by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p class="credits">Article &#169;2011 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
    <li><a href="/articles/conditions-amblyopia">Amblyopia (Lazy Eye)</a></li>
    <li><a href="/articles/conditions-astigmatism">Astigmatism</a></li>
    <li><a href="/articles/conditions-blepharitis">Blepharitis</a></li>
    <li><a href="/articles/conditions-cataracts">Cataracts</a></li>
    <li><a href="/articles/conditions-cvs">Computer Vision Syndrome</a></li>
    <li><a href="/articles/conditions-diabetic">Diabetic Retinopathy</a></li>
    <li><a href="/articles/conditions-allergies">Eye Allergies</a></li>
    <li><a href="/articles/conditions-floaters">Floaters and Spots</a></li>
    <li><a href="/articles/conditions-glaucoma">Glaucoma</a></li>
    <li><a href="/articles/conditions-hyperopia">Hyperopia</a></li>
    <li><a href="/articles/conditions-keratoconus">Keratoconus</a></li>
    <li><a href="/articles/conditions-amd">Macular Degeneration</a></li>
    <li><a href="/articles/conditions-myopia">Myopia</a></li>
    <li><a href="/articles/conditions-ocular-hypertension">Ocular Hypertension</a></li>
    <li><a href="/articles/conditions-pinkeye">Pink Eye (Conjunctivitis)</a></li>
    <li><a href="/articles/conditions-presbyopia">Presbyopia</a></li>
    <li><a href="/articles/conditions-styes" class="last">Stye</a></li>
</ul> 
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>
