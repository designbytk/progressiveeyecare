<section>
<article class="full">
<h1>Styes</h1>
<p>A stye (or hordeolum) on the eyelid occurs when a gland at the base of an eyelash becomes infected. Resembling a pimple, a stye can grow on the inside or outside of the lid.</p>
<p>Styes are not harmful to vision, and they can occur at any age.</p>
<p><b>Signs and symptoms of styes</b></p>
<p>A stye typically causes redness, tenderness and swelling at the eyelid margin, and then a small pimple appears. Sometimes just the immediate area is swollen; other times, the entire eyelid swells. In some cases, styes also can cause watery eyes, a feeling like something is in the eye (called a foreign body sensation) or increased light sensitivity.</p>
<p><b>What causes styes?</b></p>
<p>Styes are caused by staphylococcal bacteria. This bacterium is commonly found in the nose, and it's easily transferred to the eye by rubbing first your nose, then your eye.</p>
<p><b>Treatment for styes</b></p>
<p>Though styes often heal within a few days on their own, you can speed the process by applying hot compresses for 10 to 15 minutes, three or four times a day over the course of several days. This will relieve the pain and bring the stye to a head, much like a pimple. The stye ruptures and drains, then heals.</p>
<p>Never "pop" a stye like a pimple; allow it to rupture on its own. If you have frequent styes, your eye doctor may prescribe an antibiotic ointment to prevent recurrences.</p>
<p>Some styes that can form deeper inside the eyelid either disappear completely or (rarely) rupture on their own. This type of stye can be more serious, and may need to be opened and drained by your eyecare practitioner.</p>
<p><b>Chalazion: Another type of eyelid bump</b></p>
<p>Often mistaken for a stye, a chalazion (shah-LAY-zee-on) is an enlarged, blocked oil gland in the eyelid. A chalazion mimics a stye for the first few days, and then turns into a painless hard, round bump later on. Most chalazia develop further from the eyelid edge than styes.</p>
<p>Although the same treatment used for styes can speed the healing of a chalazion, the bump may linger for one to several months. If the chalazion remains after several months, your eye doctor may drain it or inject a steroid to facilitate healing.</p>
<p><i>Source: 7 Things to Know About an Eye Stye by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p class="credits">Article &#169;2011 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
    <li><a href="/articles/conditions-amblyopia">Amblyopia (Lazy Eye)</a></li>
    <li><a href="/articles/conditions-astigmatism">Astigmatism</a></li>
    <li><a href="/articles/conditions-blepharitis">Blepharitis</a></li>
    <li><a href="/articles/conditions-cataracts">Cataracts</a></li>
    <li><a href="/articles/conditions-cvs">Computer Vision Syndrome</a></li>
    <li><a href="/articles/conditions-diabetic">Diabetic Retinopathy</a></li>
    <li><a href="/articles/conditions-dry-eyes">Dry Eye Syndrome</a></li>
    <li><a href="/articles/conditions-allergies">Eye Allergies</a></li>
    <li><a href="/articles/conditions-floaters">Floaters and Spots</a></li>
    <li><a href="/articles/conditions-glaucoma">Glaucoma</a></li>
    <li><a href="/articles/conditions-hyperopia">Hyperopia</a></li>
    <li><a href="/articles/conditions-keratoconus">Keratoconus</a></li>
    <li><a href="/articles/conditions-amd">Macular Degeneration</a></li>
    <li><a href="/articles/conditions-myopia">Myopia</a></li>
    <li><a href="/articles/conditions-ocular-hypertension">Ocular Hypertension</a></li>
    <li><a href="/articles/conditions-pinkeye">Pink Eye (Conjunctivitis)</a></li>
    <li><a href="/articles/conditions-presbyopia" class="last">Presbyopia</a></li>
</ul> 
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>
