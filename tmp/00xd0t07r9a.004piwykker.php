<section>
<article class="full">
<h1>Children’s Vision FAQ</h1>
<p><b>Q:</b> Our 3-year-old daughter was just diagnosed with strabismus and amblyopia. What are the percentages of a cure at her age?</p>
<p><b>A:</b> With proper treatment, the odds are very good. Generally, it is better to treat strabismus and amblyopia in young childhood rather than waiting until after age 10. If your daughter’s eye turn (strabismus) is constant, it’s likely surgery will be necessary to straighten her eyes in order for her therapy for amblyopia (or “lazy eye”) to be successful. Strabismus surgery may be needed even if her eyes alternate in their misalignment.</p>
<p><b>Q:</b> My 5-year-old daughter passed her vision screening at school. Does she still need an eye exam?</p>
<p><b>A:</b> Yes. School vision screenings are not a substitute for a comprehensive eye exam — they are designed to detect only gross vision problems. Children can pass a screening at school and still have vision problems that can affect their learning and school performance. A comprehensive children's eye exam by an optometrist can detect vision problems a school screening may miss. Also, a comprehensive eye exam includes an evaluation of your child’s eye health, which is not part of a school vision screening.</p>
<p><b>Q:</b> My daughter (age 10) is farsighted and has been wearing glasses since age two. We think she may have problems with depth perception. How can she be tested for this, and if there is a problem, can it be treated?</p>
<p><b>A:</b> A very quick and simple in-office test called a stereopsis test can determine if your daughter has normal depth perception. In this test, she wears “3-D glasses” and looks at a number of objects in a special book or on a chart across the room. If she has reduced stereopsis, a program of vision therapy often can help improve depth perception.</p>
<p><b>Q:</b> How often should children have their eyes examined?</p>
<p><b>A:</b> Infants should have their first comprehensive eye exam at 6 months of age, and young children should have routine eye exams at age 3 and again at age 5 or 6 (just before they enter kindergarten or the first grade), says the American Optometric Association (AOA).</p>
<p>For school-aged children, the AOA recommends an eye exam every two years if no vision correction is needed. Children who need eyeglasses or contact lenses should be examined annually.</p>
<p><b>Q:</b> What is vision therapy?</p>
<p><b>A:</b> Vision therapy (sometimes called vision training) is an individualized program of eye exercises and other methods to correct vision problems other than nearsightedness, farsightedness and astigmatism. Problems treated with vision therapy include amblyopia (‘lazy eye”), eye movement and eye alignment problems, focusing problems and certain visual-perceptual disorders. Vision therapy usually is performed in an optometrist’s office, but most treatment plans also include daily vision exercises to be performed at home.</p>
<p><b>Q:</b> Can vision therapy cure learning disabilities?</p>
<p><b>A:</b> No, vision therapy cannot correct learning disabilities. However, some children with learning disabilities also have vision problems that interfere with learning. Vision therapy can correct such vision problems that may be contributing to a child’s learning problems. </p>
<p><b>Q:</b> Our active 1-year-old boy needs glasses to correct his farsightedness and the tendency for his eyes to cross. But he pulls them off the second they go on. We've tried an elastic band, holding his arms, tape... He just struggles and cries. How do we get him to wear his glasses?</p>
<p><b>A:</b> This is not an uncommon problem. Persistence is the key to helping your toddler get used to the sensation of wearing glasses. Insist that he put his glasses on as soon as he wakes up in the morning – this will help him adapt to the glasses easier.</p>
<p>If your child complains of not being able to see clearly with his glasses, it may be a good idea to return to your eye doctor and/or the optical store where you purchased the glasses to make sure the prescription is correct and his glasses were made correctly and are fitting properly.</p>
<p>Sometimes, the problem may be the eyeglass frames, not the lenses. Today there are many styles of frames for young children, including some that come with an integrated elastic band to help keep them comfortably on a young child’s head.</p>
<p><b>Q:</b> We have an 11-year-old son who first became nearsighted when he was 7. Every year, his eyes get worse. Is there anything that can be done to prevent this?</p>
<p><b>A:</b> Rigid gas permeable (GP) contact lenses may help. Research shows that, in many cases, fitting myopic youngsters with GP lenses may slow the progression of their nearsightedness. There also are special fitting techniques with GP contacts called orthokeratology (ortho-k) and corneal refractive therapy (CRT) that can temporarily reverse certain amounts of myopia. Some studies suggest bifocals and/or reading glasses also may slow down the progression of myopia in some children.</p>
<p><b>Q:</b> My 7-year-old son's teacher thinks he has “convergence insufficiency.” What is this, and what can I do about it?</p>
<p><b>A:</b> Convergence insufficiency (CI) is a relatively common learning-related vision problem where a person’s eyes don’t stay comfortably aligned when they are reading or doing close work. For reading and other close-up tasks, our eyes need to be pointed slightly inward (converged). A person with convergence insufficiency has a tough time doing this, which can lead to eyestrain, headaches, fatigue, blurred vision and reading problems. Often a program of vision therapy can effectively treat CI and reduce or eliminate these problems. Sometimes, special reading glasses also can help.</p>
<p><b>Q:</b> My son is 5 years old and has 20/40 vision in both eyes. Should I be concerned, or could this improve with time?</p>
<p><b>A:</b> Usually, 5-year-olds can see 20/25 or better. But keep in mind that visual acuity testing is a subjective matter – during the test, your child is being asked to read smaller and smaller letters on a wall chart. Sometimes, kids give up at a certain line on the chart when they can actually read smaller letters. Other times, they may say they can’t read smaller letters because they want glasses. (Yes, this happens!)</p>
<p>Also, if your son had his vision tested at a school screening (where there can be plenty of distractions), it’s a good idea to schedule a comprehensive eye exam to rule out nearsightedness, farsightedness, astigmatism or an eye health problem that may be keeping him from having better visual acuity.</p>
<p><b>Q:</b> My daughter has been diagnosed with refractive amblyopia due to severe farsightedness in one eye. She just got her glasses and the lens for her bad eye is much thicker than the other lens. She complains that the glasses make her dizzy and she refuses to wear them. Can anything be done about this?</p>
<p><b>A:</b> In situations like this, where one eye needs a much stronger correction than the other, contact lenses are a better eyewear solution. With glasses, the unequal lens powers cause an unequal magnification effect, so the two eyes form images in the brain that are different in size. This can cause discomfort (and even dizziness and nausea) because the brain may not be able to blend the two separate images into a single, three-dimensional one. And, of course, the glasses are unattractive because one lens is much thicker than the other.</p>
<p>Even if your child is quite young, she can probably handle contact lens wear. Contact lenses of unequal power don’t cause the differences in image magnification that glasses do. Continuous wear lenses (worn day and night for up to 30 days, then discarded) or one-day disposable contact lenses may be good options.</p>
<p>Keep in mind that amblyopia is a condition where one eye doesn’t see as well as the other, even with the best possible correction lens in place. Simply wearing the contacts may not improve the vision in her weak eye. Usually a program of vision therapy also will be needed.</p>
<p><i>Source:  Children’s Vision Problems Q&amp;A by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a></i></p>
<p class="credits">Article ©2011 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
	<ul>
	    <li><a href="/articles/children-learning">Learning-Related Vision Problems</a></li>
	    <li><a href="/articles/children-infants" class="last">Your Infant's Visual Development</a></li>
	</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>
