<section>
<article class="full">
<h1>Myopia</h1>
<p>Myopia, or nearsightedness, is a very common vision problem and it's become more prevalent in recent years.</p>
<p>In fact, a recent study found that myopia now affects roughly 42 percent of the U.S. population, up from 25 percent three decades ago.*</p>
<p><b>Signs and symptoms of myopia</b></p>
<p>Nearsighted people have difficulty reading road signs and seeing distant objects clearly, but can see well for up-close tasks such as reading and computer use.</p>
<p>Other signs and symptoms of myopia include headaches or eyestrain, squinting and feeling fatigued when driving or playing sports. If you experience these symptoms while wearing your glasses or contact lenses, you may need a stronger prescription.</p>
<p><b>What causes myopia?</b></p>
<p>Myopia occurs when the eyeball is slightly longer than usual from front to back. This causes light rays to focus at a point in front of the retina, rather than directly on its surface.</p>
<p>Nearsightedness also can be caused by the clear front surface of the eye (cornea) or the lens inside the eye being too curved for an eyeball of normal length.</p>
<p>Nearsightedness runs in families and usually appears in childhood. This vision problem may stabilize at a certain point, although sometimes it worsens with age.</p>
<p><b>Myopia treatment</b></p>
<p>Myopia can be corrected with eyeglasses, contact lenses or refractive surgery. Depending on the degree of your nearsightedness, you may need to wear eyeglasses or contact lenses all the time, or only when you need sharper distance vision, like when driving, viewing a chalkboard or watching a movie.</p>
<p>If your glasses or contact lens prescription begins with minus numbers, like -2.50, you are nearsighted.</p>
<p>Refractive surgery options for myopia correction include laser procedures such as LASIK and PRK, and non-laser procedures such as corneal inserts and implantable lenses. One advantage of the non-laser options is that, although they’re intended to be permanent, they may be removed in case of a problem or change of prescription.</p>
<p>For patients who prefer non-surgical correction of myopia, there is orthokeratology. Also called "ortho-k," this is the fitting of special rigid gas permeable (GP) contact lenses for overnight wear that reshape the cornea to temporarily correct your myopia.</p>
<p>When the GP lenses are removed in the morning, the cornea temporarily retains a new, flatter shape, so you can see clearly without glasses or contacts during the day.</p>
<p>If you think you might be interested in ortho-k (or a related specialty contact lens procedure called Corneal Refractive Therapy, or CRT), ask your eye doctor about these lenses.</p>
<p></p>
<p>*Increased prevalence of myopia in the United States between 1971-1972 and 1999-2004. Archives of Ophthalmology. December 2009.</p>
<p><i>Source: Myopia (Nearsightedness) by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p>Article ©2012 Access Media Group LLC.All rights reserved.Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
    <li><a href="/articles/conditions-amblyopia">Amblyopia (Lazy Eye)</a></li>
    <li><a href="/articles/conditions-astigmatism">Astigmatism</a></li>
    <li><a href="/articles/conditions-blepharitis">Blepharitis</a></li>
    <li><a href="/articles/conditions-cataracts">Cataracts</a></li>
    <li><a href="/articles/conditions-cvs">Computer Vision Syndrome</a></li>
    <li><a href="/articles/conditions-diabetic">Diabetic Retinopathy</a></li>
    <li><a href="/articles/conditions-dry-eyes">Dry Eye Syndrome</a></li>
    <li><a href="/articles/conditions-allergies">Eye Allergies</a></li>
    <li><a href="/articles/conditions-floaters">Floaters and Spots</a></li>
    <li><a href="/articles/conditions-glaucoma">Glaucoma</a></li>
    <li><a href="/articles/conditions-hyperopia">Hyperopia</a></li>
    <li><a href="/articles/conditions-keratoconus">Keratoconus</a></li>
    <li><a href="/articles/conditions-amd">Macular Degeneration</a></li>
    <li><a href="/articles/conditions-ocular-hypertension">Ocular Hypertension</a></li>
    <li><a href="/articles/conditions-pinkeye">Pink Eye (Conjunctivitis)</a></li>
    <li><a href="/articles/conditions-presbyopia">Presbyopia</a></li>
    <li><a href="/articles/conditions-styes" class="last">Stye</a></li>
</ul> 
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>