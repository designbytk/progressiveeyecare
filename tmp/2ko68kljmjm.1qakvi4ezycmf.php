<section>
<article class="full">
<h1>Lens Options for Eyeglasses</h1>
<p>Your options in eyeglass lenses go far beyond the simple choice of conventional glass or plastic lenses.</p>
<p>Whatever you desire — thinner, lighter lenses; impact-resistant lenses; or lenses that eliminate reflections and look nearly invisible — there is a lens material and design that fits the bill.</p>
<p><b>Thinner, lighter "high-index" lenses</b></p>
<p>Nearly everyone can benefit from thinner, lighter lenses. High-index plastic lenses can be up to 50 percent thinner than regular glass or plastic lenses, and they’re usually much lighter, too.</p>
<p>Though high-index lenses are especially beneficial if you have a strong eyeglasses prescription, they can make a noticeable difference in the appearance of virtually any pair of glasses. High-index lenses bend light more efficiently than regular glass or plastic lenses, so less lens material is required to correct your vision.</p>
<p>There is a range of high-index lenses to choose from, each having a different price point based on how much thinner the lenses are compared with regular plastic lenses. All high-index lenses are classified by their “index of refraction” (or “refractive index”).</p>
<p>Generally, lenses with a higher index of refraction will be thinner (and usually more expensive) than lenses with a lower index. The index of refraction of regular plastic lenses is 1.50. The refractive index of high-index plastic lenses ranges from 1.53 to 1.74. Those in the range of 1.53 to 1.59 are about 20 percent thinner than regular plastic lenses, whereas 1.74 high index lenses are up to 50 percent thinner than regular plastic lenses.</p>
<p>High-index lenses are available in virtually all lens designs (single vision, bifocal, progressive, photochromic, etc.) and your professional optician can advise you regarding the combination of lens material and design that best fits your needs and budget. </p>
<p><i>Note: High-index lenses reflect more light than regular glass or plastic lenses, so anti-reflective (AR) coating is highly recommended for these lenses (see below).</i></p>
<p><b>Aspheric lenses for a slim profile</b></p>
<p>To make high-index lenses even more attractive, most of them have an “aspheric” design. This means that instead of having a round (or “spherical”) curve on the front surface, these lenses have a curve that gradually changes from the center to the lens to the periphery. This makes aspheric lenses noticeably flatter for a slimmer, more attractive lens profile.</p>
<p>Aspheric lenses are particularly beneficial if you are farsighted. The flatter profile of aspheric lenses greatly reduces the magnified, “bug-eye” look caused by regular, highly curved lenses for farsightedness, and they greatly reduce the “bulge” of the lenses from the frame.</p>
<p>Because of their slim profile, aspheric lenses also have less mass, making them much lighter than conventional eyeglass lenses. Aspheric lenses also provide superior peripheral vision compared with conventional lenses.</p>
<p><i>Note: Because they have flatter curves than regular lenses, aspheric lenses may cause more noticeable reflections. Anti-reflective (AR) coating is recommended for these lenses (see below).</i></p>
<p><b>Polycarbonate and Trivex lenses: Tough as nails</b></p>
<p>Polycarbonate and Trivex lenses are special lightweight high-index lenses that offer superior impact resistance. These lenses are up to 10 times more impact resistant than regular plastic lenses, making them an ideal choice for children’s eyewear, safety glasses, and for anyone with an active lifestyle who wants a thinner, lighter, safer lens.</p>
<p>Polycarbonate lenses have a refractive index of 1.59, making them 20 to 25 percent thinner than regular plastic lenses. They also are up to 30 percent lighter than regular plastic lenses, making them a good choice for anyone who is sensitive to the weight of eyeglasses on their nose.</p>
<p>Trivex lenses may be slightly thicker than polycarbonate lenses in some prescriptions, but they provide comparable impact resistance and, like polycarbonate lenses, they block 100 percent of the sun’s harmful UV rays.</p>
<p><b>Anti-reflective coating: A benefit for all lenses</b></p>
<p>All eyeglass lenses reflect some light, reducing the amount of light that enters the eye to form visual images. This can affect your vision, especially under low-light conditions such as driving at night. Lens reflections also cause glare, further reducing vision in these situations.</p>
<p>The amount of light reflected from eyeglass lenses depends on the lens material. Conventional glass or plastic lenses reflect about 8 percent of incident light, so only 92 percent of available light enters the eye for vision. Thinner, lighter high-index lenses reflect up to 50 percent more light than regular glass or plastic lenses and therefore can cause more problems with glare unless something is done to reduce reflections.</p>
<p>Anti-reflective (AR) coating reduces lens reflections and allows more light to enter the eye for better night vision. Regardless of the lens material, eyeglass lenses with AR coating transmit more than 99 percent of available light to the eye.</p>
<p>By eliminating surface reflections, anti-reflective coating also makes your lenses nearly invisible. This greatly improves the appearance of your eyewear and allows others to see your eyes, not the reflections in your glasses.</p>
<p>Be sure to use only products recommended by your optician when cleaning lenses with anti-reflective coating. Because AR coating eliminates reflections that hide small scratches, you’ll want to take care not to scratch AR-coated lenses, as scratches on these lenses may be more visible than scratches on uncoated lenses.</p>
<p><b>Scratch-resistant coatings</b></p>
<p>No eyeglass lenses — not even glass lenses — are scratch-proof. However, lenses that are treated front and back with a clear, hard coating are more resistant to scratching, whether it's from dropping your glasses on the floor or occasionally cleaning them with a paper towel.</p>
<p>Kids' lenses, especially, benefit from scratch-resistant coatings.</p>
<p>Nearly all high-index lenses (including polycarbonate) come with a factory-applied scratch-resistant coating for added durability. This coating is optional for regular plastic lenses. However, to safeguard your eyewear investment, scratch-resistant coating should be considered for all eyeglass lenses. The only exception is glass lenses, which are naturally hard and scratch-resistant.</p>
<p>To further protect your eyeglasses from scratches, keep them in a protective case when you’re not wearing them. Also, never clean your lenses without first rinsing them with water or an approved cleaning solution. Rubbing a dry, dusty or dirty lens with a cleaning cloth or towel can cause scratches, even if the lens is treated with a scratch-resistant coating.</p>
<p><b>Ultraviolet (UV) treatment</b></p>
<p>Just as you use sunscreen to keep the sun's UV rays from harming your skin, UV treatment for eyeglass lenses blocks those same rays from damaging your eyes. Overexposure to ultraviolet light is thought to be a cause of cataracts, retinal damage and other eye problems.</p>
<p>Most high-index lenses have 100 percent UV protection built-in. But with regular plastic lenses, a lens treatment is required for these lenses to block all UV rays. This UV treatment does not change the appearance of the lenses and is quite inexpensive.</p>
<p><b>Photochromic lenses: Sun-sensitive lenses</b></p>
<p>Photochromic lenses are convenient indoor-outdoor eyeglass lenses that automatically darken to a sunglass shade outside when exposed to sunlight, and then quickly return to a clear state indoors. Photochromic lenses also provide 100 percent protection from the sun’s UV rays and are available in a wide variety of lens materials and designs, including bifocal and progressive lenses.</p>
<p>The amount of darkening that most photochromic lenses undergo depends on how much UV radiation they are exposed to. As a general rule, these lenses won't get as dark inside a car or truck because the glass windshield blocks out much of the sun’s UV rays that cause the lenses to change color.</p>
<p>For driving on sunny days, polarized sunglasses usually are the best solution to reduce glare and improve visibility.</p>
<p><b>Your optician can help you choose</b></p>
<p>With so many new lens products available, it’s hard to know all your options and decide which lenses are best for you. A professional optician can make selecting your eyeglasses easy and fun and help you find the perfect eyewear for your personal style and vision requirements.</p>
<p><i>Source: Eyeglass Lens Coatings by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p class="credits">Article &#169;2011 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
	<ul>
	<li><a href="/articles/eyeglasses-basics">The Basics of Eyeglasses</a></li>
    <li><a href="/articles/eyeglasses-frames-materials">Eyeglass Frame Materials</a></li>
    <li><a href="/articles/eyeglasses-specialty" class="last">Specialty Eyewear</a></li>
</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>