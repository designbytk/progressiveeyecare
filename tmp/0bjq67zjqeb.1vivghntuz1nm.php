<!-- ADD Facebook Call-to-Action 5-21-2016 -->
<?php if ($currentpage=='home'): ?>
  <?php else: ?>
    <div class="facebook-side">
      <img alt="facebook, deals, frames"
           src="<?php echo $site; ?>/img/facebook-call-to-action.gif" />
      <p>
        <em>Visit our Facebook page for current promotions, offers, and to view the latest frame styles.</em>
      </p>
      <a href="https://www.facebook.com/Progressive-Eye-Care-168939389788564/">Check It Out</a>
    </div>
    <div class="appointment">
      <h2 class="pull-left">
      <i class="icon-calendar pull-left"></i>Appointments</h2>
      <a href="https://reviews.solutionreach.com/vs/progressive_eye_care_farmington/appt"
         target="_blank">Request An Appointment</a>
    </div>
  
<?php endif; ?>
<!-- END Add -->
<?php if ($currentpage=='forms'): ?>
  <?php else: ?>
    <div class="forms clearfix">
      <a class="dlforms"
         href="http://dl.progressiveeyecare.net/pecct-forms.pdf">
        <img alt="Download Forms"
             src="<?php echo $site; ?>/img/download-forms.png" />
      </a>
      <h2>
      <i class="icon-file pull-left"></i>Patient Forms</h2>
      <p>Download our current patient registration forms 
      <a href="#pecctform"
         id="formdl">here</a></p>
    </div>
    <!-- Lightbox Images -->
    <?php echo $this->render('/pages/forms.html',$this->mime,get_defined_vars()); ?>
  
<?php endif; ?>
<?php if ($currentpage=='contact'): ?>
  <?php else: ?>
    <div class="locations">
      <h2>
      <i class="icon-map-marker pull-left"></i>Locations</h2>
      <b>Farmington</b>
      <p>Talcott Plaza</p>
      <p>230 Farmington Ave.</p>
      <b>Southbury</b>
      <p>One Reservoir Office Park</p>
      <p>1449 Old Waterbury Rd., Suite 304</p>
      <?php if ($currentpage=='home'): ?>
        <?php else: ?>
          <a class="more"
             href="/contact">
          <i class="icon-location-arrow pull-left"></i>Get Directions</a>
        
      <?php endif; ?>
    </div>
  
<?php endif; ?>
<?php if ($currentpage=='about'): ?>
  
    <div class="hours">
      <h2>
      <i class="icon-time pull-left"></i>Our Hours</h2>
      <b>Farmington</b>
      <p>
      <span>10:00 am - 6:00 pm</span>Tue</p>
      <p>
      <span>10:00 am - 8:00 pm</span>Wed</p>
      <p>
      <span>10:00 am - 5:00 pm</span>Thu</p>
      <p>
      <span>10:00 am - 3:00 pm</span>Fri</p>
      <p>
      <span>9:00 am - 2:00 pm</span>Sat</p>
      <b>Southbury</b>
      <p>
      <span>11:00 am - 7:00 pm</span>Tue &amp; Wed</p>
      <p>
      <span>9:00 am - 5:00 pm</span>Thur &amp; Fri</p>
    </div>
  
  <?php else: ?>
    <?php if ($currentpage=='home'): ?>
      <?php else: ?>
        <div class="doctors">
          <h2>
          <i class="icon-user-md pull-left"></i>Meet Our Doctors</h2>
          <p>Our staff is always willing to help and assist with all of your optical needs. We look forward to seeing you in the future and making you part of our optical family.</p>
          <a class="more"
             href="/about">
          <i class="icon-file-text-alt pull-left"></i>Find out more</a>
        </div>
      
    <?php endif; ?>
  
<?php endif; ?>
<?php if ($currentpage=='services'): ?>
  
    <div class="insurance">
      <h2>
      <i class="icon-umbrella pull-left"></i>Insurance</h2>
      <p>We work with the following insurance carriers:</p>
      <p>VSP, Medicare, Anthem Vision, Blue Cross Blue Shield, Aetna, United Healthcare, Connecticare, Cigna, as well as most other major insurance carriers. Feel free to call us to see if we participate with your insurance, we'll be happy to let
      you know.</p>
      <p>
        <em>Check with our staff if you have other types of coverage.</em>
      </p>
    </div>
    <div class="payment">
      <h2>
      <i class="icon-money pull-left"></i>Payment</h2>
      <p>We accept Personal Checks, MasterCard®, Visa®, and Discover®.</p>
      <img alt="Credit Cards"
           src="../img/payment-cards.jpg" />
    </div>
  
<?php endif; ?>
