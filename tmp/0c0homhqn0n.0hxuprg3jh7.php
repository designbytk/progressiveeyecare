<section>
<article class="full">
<h1>Blepharitis</h1>
<p>Blepharitis is inflammation of the eyelids, characterized by red, itchy eyelids and the formation of dandruff-like scales at the base of the lashes. It is a common disorder and affects people of all ages.</p>
<p>Blepharitis is not contagious and generally does not cause permanent vision problems.</p>
<p>There are two forms of blepharitis: anterior blepharitis and posterior blepharitis.</p>
<p>Anterior blepharitis affects the front of the eyelids, usually near the eyelashes. The two most common causes of anterior blepharitis are bacteria and a skin disorder called seborrheic dermatitis, which causes itchy, flaky red skin.</p>
<p>Posterior blepharitis affects the inner surface and edge of the eyelid that comes in contact with the eye. It usually is caused by problems with the oil (meibomian) glands in the lid margin, leading to excessive bacterial growth. Posterior blepharitis also can be associated with acne rosacea and dandruff of the scalp.</p>
<p><b>Blepharitis signs and symptoms</b></p>
<p>Signs and symptoms of blepharitis include eye irritation; redness at the eyelid margins; burning; tearing; foreign body sensation (feeling something is "in" your eye); dry eyes; and the formation of crusty debris on your eyelashes, in the corner of your eyes or on your eyelids.</p>
<p>If you have any signs or symptoms of blepharitis, see your eye doctor for an exam and possible treatment. Left untreated, blepharitis can lead to unsightly and uncomfortable styes, eye infections and even corneal ulcers.</p>
<p><b>Blepharitis treatments</b></p>
<p>The treatment your eye doctor prescribes will depend on the type of blepharitis you have. Blepharitis treatment may include simple measures such as applying warm compresses to your eyelids, cleaning your eyelids more frequently, and massaging your lids to help express oil from the meibomian glands.</p>
<p>If blepharitis makes your eyes feel dry, artificial tears or lubricating ointments also may be recommended. In some cases, anti-bacterial or steroid eye drops or ointments may be prescribed.</p>
<p>When treating blepharitis, always wash your hands before and after touching your eyelids. Your eye doctor will provide instructions on the products and techniques to use to relieve symptoms and get your blepharitis under control.</p>
<p>Some research suggests nutritional supplements such as fish oil and flaxseed oil that contain omega-3 fatty acids may help prevent or reduce the severity of posterior blepharitis. Be sure to discuss any supplement use with your doctor.</p>
<p>Blepharitis typically is a chronic condition, and diligent daily eyelid hygiene may be needed to prevent reoccurrences.</p>
<p>If you normally wear contact lenses, you may need to discontinue wearing them until all signs and symptoms of blepharitis have been eliminated. Sometimes, changing from soft contact lenses to rigid gas permeable (GP) contacts can be helpful, since GP lenses are less likely to accumulate bacteria-containing deposits that can cause blepharitis. Other blepharitis prevention measures include replacing soft contact lenses more frequently or changing to one-day disposable contacts.</p>
<p><i>Source: Blepharitis by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p class="credits">Article &#169;2011 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
    <li><a href="/articles/conditions-amblyopia">Amblyopia (Lazy Eye)</a></li>
    <li><a href="/articles/conditions-astigmatism">Astigmatism</a></li>
    <li><a href="/articles/conditions-cataracts">Cataracts</a></li>
    <li><a href="/articles/conditions-cvs">Computer Vision Syndrome</a></li>
    <li><a href="/articles/conditions-diabetic">Diabetic Retinopathy</a></li>
    <li><a href="/articles/conditions-dry-eyes">Dry Eye Syndrome</a></li>
    <li><a href="/articles/conditions-allergies">Eye Allergies</a></li>
    <li><a href="/articles/conditions-floaters">Floaters and Spots</a></li>
    <li><a href="/articles/conditions-glaucoma">Glaucoma</a></li>
    <li><a href="/articles/conditions-hyperopia">Hyperopia</a></li>
    <li><a href="/articles/conditions-keratoconus">Keratoconus</a></li>
    <li><a href="/articles/conditions-amd">Macular Degeneration</a></li>
    <li><a href="/articles/conditions-myopia">Myopia</a></li>
    <li><a href="/articles/conditions-ocular-hypertension">Ocular Hypertension</a></li>
    <li><a href="/articles/conditions-pinkeye">Pink Eye (Conjunctivitis)</a></li>
    <li><a href="/articles/conditions-presbyopia">Presbyopia</a></li>
    <li><a href="/articles/conditions-styes" class="last">Stye</a></li>
</ul> 
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>