<section>
<article class="full">
<h1>Eyeglass Frame Materials</h1>
<p>At one time, choosing eyeglass frames was a simple matter of selecting metal or plastic frames in a size and shape that fit your face.</p>
<p>Today you have many more options in eyeglass frames based on factors such as color, hypoallergenic materials, durability, weight, price and uniqueness.</p>
<p><b>Metal frames</b></p>
<p>Most people who purchase eyeglasses prefer the timeless appearance of metal frames. There are many types of metal you can choose, and each has its own distinctive properties.</p>
<p><b>Stainless steel.</b> This is an iron-carbon alloy that also contains chromium. Stainless steel frames are lightweight, strong, durable, flexible and corrosion-resistant. They can be produced in matte or polished finishes.</p>
<p><b>Titanium.</b> This premium metal is very strong, durable, corrosion-resistant, and is 40% lighter than other metals. It’s also hypoallergenic, making it titanium frames an excellent choice for anyone with sensitive skin. Titanium frames are available in several colors.</p>
<p><b>Beta titanium.</b> This is an alloy of predominantly titanium, with small amounts of aluminum and vanadium. These other metals in the alloy make beta titanium more flexible than frames made of pure titanium for easier fitting adjustments.</p>
<p><b>Memory metal.</b> This is a titanium alloy composed of roughly 50 percent titanium and 50 percent nickel. Memory metal eyeglass frames are extremely flexible, and can be twisted or bended and still return to their original shape. This feature makes memory metal frames a great choice for kids and anyone who is hard on their glasses.</p>
<p><b>Beryllium</b>. This lower-cost alternative to titanium resists corrosion and tarnishing, making it an excellent choice for anyone with high skin acidity or who spends a good amount of time in or around salt water. Beryllium frames also are lightweight, strong, flexible and are available in a wide range of colors.</p>
<p><b>Monel.</b> This popular, inexpensive material is an alloy of nickel and copper. Monel is less costly than other metals, but – depending on the quality of the plating used – monel frames can cause skin reactions in some individuals over time.</p>
<p><b>Plastic frames</b></p>
<p><b>Zyl. </b>This very popular frame material (also called Zylonite or cellulose acetate) is a lightweight and relatively inexpensive type of plastic. Zyl frames are available in a wide variety of colors, including multi-colored models and frames with different layers of color.</p>
<p><b>Propionate</b>. This is a nylon-based plastic that is strong, flexible, lightweight and hypoallergenic. Propionate often is used in frames for sports eyeglasses because of its durability.</p>
<p><b>Nylon. </b>This frame material<b> </b>is still occasionally used. Nylon is strong, lightweight and flexible, but it can become brittle with age. For this reason, it has for the most part been replaced by nylon blends – polyamides, copolyamides and gliamides – which are more durable.</p>
<p><b>Combination frames</b></p>
<p>Combination frames have both metal and plastic components. Popular in the 1950s and 1960s, combination frames have made a comeback recently, in a wide variety of colors that give classic models a more modern look.</p>
<p><b>Choose more than one style</b></p>
<p>Each eyeglass frame material offers its own advantages and style features. For eyewear that fits every occasion in your life, consider purchasing more than one pair of glasses and choose a different frame material for each pair.</p>
<p>For example, you may want one pair of glasses with metal frames for a conservative, professional look for work. But on weekends, you may want something with more color or style, like a zyl frame in laminated colors, or a combination frame with a modern spin of a classic retro-style.</p>
<p><i>Source: Eyeglass Frame Materials by <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.</i></p>
<p class="credits">Article &#169;2011 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
	<ul>
	<li><a href="/articles/eyeglasses-basics">The Basics of Eyeglasses</a></li>
    <li><a href="/articles/eyeglasses-lenses">Lens Options for Eyeglasses</a></li>
    <li><a href="/articles/eyeglasses-specialty" class="last">Specialty Eyewear</a></li>
</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>	
</aside>
