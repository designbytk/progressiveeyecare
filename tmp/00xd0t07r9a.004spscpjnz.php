<!DOCTYPE html>
<html lang="en">
<?php echo $this->render('gui/head.html',$this->mime,get_defined_vars()); ?>	

<body>
<?php echo $this->render('gui/header.html',$this->mime,get_defined_vars()); ?>
<figure>
<div>
	<h2><i class="icon-chevron-sign-right"></i>ARTICLES</h2>
</div>
</figure>	
<section id="content">
<div>
	<?php echo $this->render($currentarticle,$this->mime,get_defined_vars()); ?>	
	</div>
</section> 
<?php echo $this->render('gui/footer.html',$this->mime,get_defined_vars()); ?>	

</body>
</html>
                                                      