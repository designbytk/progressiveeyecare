<!DOCTYPE html>
<html lang="en">
<?php echo $this->render('gui/head.html',$this->mime,get_defined_vars()); ?>	
<body>
	<?php echo $this->render('gui/header.html',$this->mime,get_defined_vars()); ?>
	<figure>
	<div class="slider">
		<div class="fs_loader"></div>
			<div class="slide">
				<img 	src="img/slider-img/lime-circles-1.png"
						alt="Lime Circles Movement"
						width="738" height="738"			
						data-position="20,0" data-in="bottom" data-delay="200" data-out="top">
				<img 	src="img/slider-img/focus-eye.png"
						alt="Human Eye Focused"
						width="500" height="360"			
						data-position="40,70" data-in="bottom" data-delay="280" data-out="fade">
				<img 	src="img/slider-img/med-blue-circles-1.png"
						alt="Medium Blue Circles Movement"
						width="495" height="495"			
						data-position="-200,605" data-in="top" data-delay="360" data-out="bottom">
				<p 		data-position="25,785" data-in="top" data-step="0" data-delay="400">We are</p>
				<h2 	data-position="60,690" data-in="right" data-step="0" data-delay="400">Focused</h2>
				<img 	src="img/slider-img/focus-girl.png"
						alt="Young Woman Smiling with Glasses"
						width="304" height="274"			
						data-position="62,460" data-in="left" data-step="1" data-delay="60" data-out="left">
				<p 		data-position="110,690" data-in="bottom" data-step="1" data-delay="200">on You and Your</p>
				<h2 	data-position="145,690" data-in="bottom" data-step="2" data-delay="0">EYE CARE</h2>
				<img 	src="img/slider-img/focus-scope.png"
						alt="Retina Scope"
						width="120" height="58"			
						data-position="190,785" data-in="bottom" data-step="2" data-delay="60" data-out="left">
			</div>
			<div class="slide">
				<img 	src="img/slider-img/blue-white-circles.png"
						alt="Blue Circles Movement"
						width="498" height="498"			
						data-position="-200,0" data-in="top" data-delay="200" data-out="bottom">
				<img 	src="img/slider-img/green-circles.png"
						alt="Green Circles Movement"
						width="601" height="601"			
						data-position="-30,499" data-in="bottom" data-delay="360" data-out="top">
				<img 	src="img/slider-img/brands-frames.png"
						alt="Eye Glasses in Hand"
						width="480" height="293"			
						data-position="120,600" data-in="bottom" data-delay="280" data-out="fade">
				<img 	src="img/slider-img/green-diagnol.png"
						alt="Green Diagnol Lines"
						width="456" height="456"			
						data-position="200,360" data-in="left" data-delay="360" data-out="bottom">
				<p 		data-position="25,60" data-in="top" data-step="0" data-delay="400">We carry the</p>
				<img 	src="img/slider-img/brands-logos.png"
						alt="Eye Glass Frame Brands"
						width="300" height="187"			
						data-position="26,642" data-in="top" data-delay="460" data-out="bottom">
				<h2 	data-position="62,60" data-in="right" data-step="0" data-delay="400">BRANDS</h2>
				<p 		data-position="110,60" data-in="bottom" data-step="1" data-delay="200">Guaranteed to Fit</p>
				<img 	src="img/slider-img/brands-woman.jpg"
						alt="Woman wearing Eye Glasses"
						width="180" height="120"			
						data-position="200,140" data-in="left" data-step="2" data-delay="360" data-out="bottom">
				<img 	src="img/slider-img/brands-man.jpg"
						alt="Man wearing Eye Glasses"
						width="180" height="120"			
						data-position="200,340" data-in="left" data-step="2" data-delay="180" data-out="bottom">
				<h2 	data-position="145,60" data-in="bottom" data-step="2" data-delay="0">Your Style</h2>	
			</div>
			<div class="slide">
				<img 	src="img/slider-img/lime-circles-1.png"
						alt="Lime Circles Movement"
						width="738" height="738"			
						data-position="20,0" data-in="bottom" data-delay="200" data-out="top">
				<img 	src="img/slider-img/family-grandpa.png"
						alt="Older Gentleman with Glasses"
						width="337" height="272"			
						data-position="70,0" data-in="bottom" data-delay="280" data-out="fade">
				<img 	src="img/slider-img/med-blue-circles-1.png"
						alt="Medium Blue Circles Movement"
						width="495" height="495"			
						data-position="-200,605" data-in="top" data-delay="360" data-out="bottom">
				<p 		data-position="25,705" data-in="top" data-step="0" data-delay="400">Quality</p>
				<h2 	data-position="60,690" data-in="right" data-step="0" data-delay="400">EYE CARE</h2>
				<img 	src="img/slider-img/family-together.png"
						alt="Mother and Daughter with Smiles"
						width="420" height="351"			
						data-position="62,280" data-in="left" data-step="1" data-delay="60" data-out="left">
				<p 		data-position="110,720" data-in="bottom" data-step="1" data-delay="200">for the entire</p>
				<h2 	data-position="145,720" data-in="bottom" data-step="2" data-delay="0">Family</h2>
				<img 	src="img/slider-img/family-book.png"
						alt="Woman Peering over Book, Reading Glasses"
						width="260" height="192"			
						data-position="210,660" data-in="bottom" data-step="2" data-delay="200" data-out="left">
			</div>
			<div class="slide">
				<img 	src="img/slider-img/blue-white-circles.png"
						alt="Blue and White Circles Movement"
						width="498" height="498"			
						data-position="-200,0" data-in="top" data-delay="200" data-out="bottom">
				<img 	src="img/slider-img/green-circles.png"
						alt="Green Circles Movement"
						width="601" height="601"			
						data-position="-30,499" data-in="bottom" data-delay="360" data-out="top">
				<img 	src="img/slider-img/contacts-finger.png"
						alt="Close up Finger with Contact Lens"
						width="480" height="332"			
						data-position="60,640" data-in="bottom" data-delay="280" data-out="fade">
				<img 	src="img/slider-img/green-diagnol.png"
						alt="Green Diagnol Movement"
						width="456" height="456"			
						data-position="200,360" data-in="left" data-delay="360" data-out="bottom">
				<h2 	data-position="62,60" data-in="right" data-step="0" data-delay="400">IN TOUCH</h2>
				<p 		data-position="110,60" data-in="bottom" data-step="1" data-delay="200">when it comes to</p>
				<img 	src="img/slider-img/contacts-case.jpg"
						alt="Contact Lenses in Lens Case"
						width="180" height="120"			
						data-position="200,140" data-in="left" data-step="2" data-delay="360" data-out="bottom">
				<img 	src="img/slider-img/contacts-eye.jpg"
						alt="Up close Human Eye, Woman"
						width="180" height="120"			
						data-position="200,340" data-in="left" data-step="2" data-delay="180" data-out="bottom">
				<h2 	data-position="145,60" data-in="bottom" data-step="2" data-delay="0">Contact Lenses</h2>	
			</div>
			
		</div>
	</figure>

	<section id="content" class="clearfix">
		<div>
		<!-- ADDED Facebook Plug 5/21/2016 -->
		<?php echo $this->render('gui/facebook.html',$this->mime,get_defined_vars()); ?>
		<!-- END Facebook Plug -->
			<section>
				<article>
				<h1>Meet Our Doctors</h1>
				<img src="img/about-home-hdr.jpg" alt="Our Doctors" />
				<p>Our doctors and staff are always willing to help and assist with all of your optical needs. We invite you to come and see for yourself. It is our aim to help you feel at ease, because you are a welcome part of our optical family.</p>
				<a href="/about" class="more"><i class="icon-file-text-alt pull-left"></i>Find out more</a>
				</article>
				<article>
				<h1>Discover our Services</h1>
				<img src="img/services-home-hdr.jpg" alt="Eyeglasses and Eye Chart" />
				<p>We pride ourselves in providing the best, professional eye care, while giving each patient the personal care and attention they deserve. Our dedication is to bringing you both the highest quality and the most comprehensive eye care.</p>
				<a href="/services" class="more"><i class="icon-file-text-alt pull-left"></i>Discover more</a>
				</article>
			</section>
			<aside>
				<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>
			</aside>
		</div>
	</section>
<?php echo $this->render('gui/footer.html',$this->mime,get_defined_vars()); ?>
<div class="info" itemscope itemtype="http://schema.org/LocalBusiness">
<a itemprop="url" href="http://pecct.com"><div itemprop="name"><strong>Progressive Eye Care</strong></div>
</a>
<div itemprop="description">Quality eye care professionals conveniently located in Farmington and Southbury, Connecticut.</div>
<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress">Talcott Plaza, 230 Farmington Ave.</span><br>
<span itemprop="addressLocality">Farmington</span><br>
<span itemprop="addressRegion">CT</span><br>
<span itemprop="postalCode">06032</span><br>
<span itemprop="addressCountry">USA</span><br>
</div>
<a href="https://maps.google.com/maps?daddr=230+Farmington+Ave,+Farmington,+CT+06032" itemprop="map"></a><br>
<time itemprop="openingHours" datetime="Tu 10:00-18:00">Wednesday 10am-6pm</time>
<time itemprop="openingHours" datetime="Wed 10:00-20:00">Wednesday 10am-8pm</time>
<time itemprop="openingHours" datetime="Th,Fr 10:00-17:00">Thursday, Friday 10am-5pm</time>
<time itemprop="openingHours" datetime="Sa 09:00-14:00">Saturday 9am-2pm</time>
<img itemprop="image" src="http://pecct.com/img/pec-logo.jpg" alt="Progressive Eye Care Logo"/>
</div>
<?php echo $this->render('gui/scripts.html',$this->mime,get_defined_vars()); ?>

</body>
</html>