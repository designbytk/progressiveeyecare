<!DOCTYPE html>
<html lang="en">
<?php echo $this->render('gui/head.html',$this->mime,get_defined_vars()); ?>	

<body>
<?php echo $this->render('gui/header.html',$this->mime,get_defined_vars()); ?>
<figure>
<div>
	<h2><i class="icon-chevron-sign-right"></i>CONTACT</h2>
</div>
</figure>	
<section id="content">
	<div>
	<section>
<article class="locations">
	<h1><i class="icon-map-marker pull-left"></i>Farmington</h1>
	    <div id="map_farmington"></div>
		<ul>
		<li><b>Address</b></li>
		<li>Talcott Plaza</li>
		<li>230 Farmington Ave.</li>
		<li>Farmington, CT 06032</li>
		</ul>
		<ul class="split">
		<li><b>Phone</b></li>
		<li>860.674.0307</li>
		</ul>
		<ul class="split">
		<li><b>Fax</b></li>
		<li>860.677.2766</li>
		</ul>
		<ul>
		<li><b>Hours</b></li>
		<li><span>10:00 am - 6:00 pm</span>Tue</li>
		<li><span>10:00 am - 8:00 pm</span>Wed</li>
		<li><span>10:00 am - 5:00 pm</span>Thu, Fri</li>
		<li><span>9:00 am - 2:00 pm</span>Sat</li>	
		</ul>
</article>
<article class="locations">
	<h1><i class="icon-map-marker pull-left"></i>Southbury</h1>
	    <div id="map_southbury"></div>
		<ul>
		<li><b>Address</b></li>
		<li>One Reservoir Office Park</li>
		<li>1449 Old Waterbury Rd., Suite 304</li>
		<li>Southbury, CT 06488</li>
		</ul>
		<ul class="split">
		<li><b>Phone</b></li>
		<li>203.267.2020</li>
		</ul>
		<ul class="split">
		<li><b>Fax</b></li>
		<li>203.267.2021</li>
		</ul>
		<ul>
		<li><b>Hours</b></li>
		<li><span>9:00 am - 5:00 pm</span>Tue, Thu, Fri</li>
		<li><span>12:00 pm - 8:00 pm</span>Wed</li>
		<li><em>Saturday hours are by appointment only, please call ahead.</em></li>
		</ul>
</article>
<form action="/contact/send" method="post" id="ajax_form">
	<h1><i class="icon-envelope-alt pull-left"></i>Send a Message</h1>
	<a href="mailto:progressiveeyecare2@yahoo.com" class="more">progressiveeyecare2@yahoo.com</a>
	
    <fieldset>
		<label for="name">Name</label>
		<span><input type="text" name="name" id="name" /></span>
		<label for="email">Email</label>
		<span><input type="text" name="email" id="email" /></span>
		<label for="message" class="msg">Message</label>
		<span><textarea name="message" class="img"></textarea></span>
	</fieldset>
	<div class="send">
		<span><input type="submit" value="Send" class="submit" /></span>
	</div>
</form>
</section>
<aside>
<?php echo $this->render('gui/sidebar.html',$this->mime,get_defined_vars()); ?>
</aside>
	</div>
</section> 
<?php echo $this->render('gui/footer.html',$this->mime,get_defined_vars()); ?>	

</body>
</html>                                    