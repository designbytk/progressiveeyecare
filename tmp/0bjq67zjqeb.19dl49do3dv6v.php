<head>
	<meta charset="utf-8">
	<title>Progressive Eye Care | Farmington | Southbury | Connecticut</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="An experienced group of Optometrists and Opticians providing vision care, from eye nutrition, to glasses and contact lenses, in Farmington and Southbury, CT.">
    <!-- Critical CSS -->
	<style>body {color: #202020;}</style>
	<script>
		var cb = function() {
		var l = document.createElement('link'); l.rel = 'stylesheet';
		l.href = 'css/main.min.css';
		var h = document.getElementsByTagName('head')[0]; h.parentNode.insertBefore(l, h); };
		var raf = requestAnimationFrame || mozRequestAnimationFrame ||
				webkitRequestAnimationFrame || msRequestAnimationFrame;
		if (raf) raf(cb);
		else window.addEventListener('load', cb);
	</script>
	<!--<link rel="stylesheet" href="<?php echo $site; ?>/css/main.min.css" type="text/css" />-->
	<link rel="stylesheet" href="<?php echo $site; ?>/css/font-awesome.min.css">
	<!--[if IE 7]>
	<link rel="stylesheet" href="<?php echo $site; ?>/css/font-awesome-ie7.min.css">
	<![endif]-->
	<!-- google fonts -->
	<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,300|Syncopate' rel='stylesheet' type='text/css'>
	
	<!-- favicon -->
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $site; ?>/img/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $site; ?>/img/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $site; ?>/img/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $site; ?>/img/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo $site; ?>/img/favicon/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $site; ?>/img/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $site; ?>/img/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $site; ?>/img/favicon/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="<?php echo $site; ?>/img/favicon/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="<?php echo $site; ?>/img/favicon/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php echo $site; ?>/img/favicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo $site; ?>/img/favicon/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="<?php echo $site; ?>/img/favicon/favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="<?php echo $site; ?>/img/favicon/mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="<?php echo $site; ?>/img/favicon/mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="<?php echo $site; ?>/img/favicon/mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="<?php echo $site; ?>/img/favicon/mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="<?php echo $site; ?>/img/favicon/mstile-310x310.png" />

	<!-- Font Awesome Icons	-->
	<!-- <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"> -->

	<!-- typekit -->
	<script type="text/javascript" src="//use.typekit.net/mkr6xqk.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	
	
</head>