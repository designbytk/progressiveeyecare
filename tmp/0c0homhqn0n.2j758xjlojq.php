<header>
		<div>
		<h1>Progressive Eye Care</h1>
		<nav>
			<ul>
				<li><a href="/<?php echo $menu['0']; ?>"><?php echo $menu['0']; ?></a></li>
				<li class="midnav"><a href="/<?php echo $menu['1']; ?>"><?php echo $menu['1']; ?></a></li>
				<li class="midnav"><a href="/<?php echo $menu['2']; ?>"><?php echo $menu['2']; ?></a></li>
				<li><a href="/<?php echo $menu['3']; ?>"><?php echo $menu['3']; ?></a></li>
			</ul>
		</nav>
		<ul id="phones">
			<li>
				<a href="tel:18606740307">860.674.0307</a>
				<p>Farmington</p>
			</li>
			<li>
				<a href="tel:12032672020">203.267.2020</a>
				<p>Southbury</p>
			</li>
		</ul>
		</div>
	</header>