<script src="<?php echo $site; ?>/js/jquery-1.9.0.min.js"
        type="text/javascript"></script>
<!--jQuery local -->
<script src="<?php echo $site; ?>/js/jquery.leanModal.min.js"
        type="text/javascript"></script>
<!--Image Zoom - Fancy Zoom -->
<script src="<?php echo $site; ?>/js/modernizr.custom.43202.js"></script>
<!-- SLIDER     -->
<?php if ($currentpage=='home'): ?>
  
    <script charset="utf-8"
            src="<?php echo $site; ?>/js/jquery.fractionslider.min.js"
            type="text/javascript"></script>
    <script charset="utf-8"
            src="<?php echo $site; ?>/js/fractionslider.js"
            type="text/javascript"></script>
  
<?php endif; ?>
<!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<script type="text/javascript">$(document).ready(function() { $("#formdl").leanModal({ closeButton: ".modal_close" }); //$('#formdl').fancyZoom({scaleImg: false, closeOnClick: true, width:200, height:100}); });</script>
<?php if ($currentpage=='contact'): ?>
  
    <!-- contact validation -->
    <script src="<?php echo $site; ?>/js/jquery.form.js"
            type="text/javascript"></script>
    <!-- google map -->
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script>//Farmington var farmington=new google.maps.LatLng(41.737728, -72.795779); var marker_f=new google.maps.Marker({ position:farmington, url: 'https://maps.google.com/maps?daddr=230+Farmington+Ave,+Farmington,+CT+06032', title: 'Get
    Directions', animation:google.maps.Animation.DROP }); //Southbury var southbury=new google.maps.LatLng(41.493493,-73.166838); var marker_s=new google.maps.Marker({ position:southbury, url:
    'https://maps.google.com/maps?daddr=1449+Old+Waterbury+Rd,+Southbury,+CT', title: 'Get Directions', animation:google.maps.Animation.DROP }); function initialize() { //Farmington var mapProp_f = { center:farmington, zoom: 17, draggable: false,
    scrollwheel: false, mapTypeId:google.maps.MapTypeId.ROADMAP }; var map_f=new google.maps.Map(document.getElementById("map_farmington"),mapProp_f); marker_f.setMap(map_f); //Southbury var mapProp_s = { center:southbury, zoom: 17, draggable:
    false, scrollwheel: false, mapTypeId:google.maps.MapTypeId.ROADMAP }; var map_s=new google.maps.Map(document.getElementById("map_southbury"),mapProp_s); marker_s.setMap(map_s); } google.maps.event.addDomListener(window, 'load', initialize);
    google.maps.event.addListener(marker_f, 'click', function() {window.open(marker_s.url);}); google.maps.event.addListener(marker_s, 'click', function() {window.open(marker_s.url);});</script>
  
<?php endif; ?>
