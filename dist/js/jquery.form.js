$(document).ready(function(){
	
	$('form#ajax_form .submit').click(function(){

		$('#ajax_form .error').hide();	//if error visibile, hide on new click
		
		var name = $('input#name').val();
		if (name == "" || name == " " || name == "Name") {
		    //$('input#name').focus().before('<div class="error">Hey, what is you name!?</div>');
		    $('fieldset').focus().before('<div class="error"><i class="icon-exclamation-sign pull-left"></i>Please provide your name.</div>');
		    return false;
		}
		
		var email_test = /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
		var email = $('input#email').val();
		if (email == "" || email == " ") {
		   $('fieldset').focus().before('<div class="error"><i class="icon-exclamation-sign pull-left"></i>Please provide your email address.</div>');
		   return false;
		} else if (!email_test.test(email)) {
		   $('fieldset').select().before('<div class="error"><i class="icon-exclamation-sign pull-left"></i>Please provide a valid email address.</div>');
		   return false;
		}
		
		var message = $('.img').val();
		if (message == "" || message == " " || message == "Message") {
		    $('fieldset').focus().fadeIn('slow').before('<div class="error"><i class="icon-exclamation-sign pull-left"></i>Please provide a brief message.</div>');
		    return false;
		}
		
		var data_string = $('form#ajax_form').serialize();

		$.ajax({
		    type:       "POST",
		    url:        "/contact/send",
		    data:       data_string,
		    success:    function() {

		$('form#ajax_form').slideUp('fast').before('<article id="success" class="full"></article>');
		$('#success').html('<h1><i class="icon-ok-sign"></i> Success</h1><p>Your message has been successfully sent, thank you.</p>').slideDown(9000);

		    }//end success function


		}) //end ajax call

		return false;


	}) //end click function
	
})