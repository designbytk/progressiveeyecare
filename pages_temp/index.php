<!--OLDER F3 Version-->
<?php

require_once 'lib/base.php';

F3::set('DEBUG',1);
F3::set('site','http://progressive.viatk.com'); 


F3::route('GET /', 
  function() {
  	F3::set('page','home');
  	F3::set('menu',array('about','services','works','contact'));
    echo Template::serve('pages/home.html');
  }
);

F3::route('GET /home', 
  function() {
    F3::reroute('/');
  }
);

F3::route('GET /@page',
  function() {
    $page = F3::get('PARAMS["page"]');
    //echo Template::serve("pages/{$page}.html");
   
    if ($page == "articles") {
    F3::set('menu',array('home','services','contact','about'));
    echo Template::serve("pages/{$page}.html");
       } else {
    F3::set('menu',array('home','services','articles','contact'));
       	echo Template::serve("pages/{$page}.html");
       }
  }
);

F3::route('GET /articles/@article', 
  function() {
  	  $article = F3::get('PARAMS["article"]');
  	  F3::set('article', $article);
  	  F3::set('menu',array('home','services','contact','about'));
      echo Template::serve('pages/article.html');
  }
);

F3::run();

?>