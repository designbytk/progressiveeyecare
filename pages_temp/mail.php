$f3->route('GET /contact/smtptest',
        function() use($f3) {

/* 
SMTP Class instantiation parameters:  
mail server (IP or FQDN, default=localhost), 
port # (default=25), 
encoding type (default=NULL), 
smtp server login username (default=NULL), 
smtp login password (default=NULL)
*/
// instantiate smtp object
            $smtp = new SMTP('mail.domain.com',25,NULL,'stu@domain.com','mypasswerd');
// set email headers
            $smtp->set('Content-type', 'text/html; charset=UTF-8'); // email will be rendered as html content
            $smtp->set('From','stu@domain.com');
            $smtp->set('To','stuw@domain.com');
            $smtp->set('Subject','F3 email test');
// use this method to render an F3 template as the body of your message
            $smtp_msg = $app->serve('template/email_template.htm'); // /gui/template/email_template.htm 
// it will render your template, replacing all F3 tags and commands

// send email message
            $smtp->send($smtp_msg);
// view the conversation between F3 and the mail server for debugging
            echo "DEBUG=" . $smtp->log . "<hr>" ;

} // end function

);  // end route statement