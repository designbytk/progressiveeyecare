<section>
<article class="full">
	<h1>
		LASIK Risks and Complications
	</h1>
	<p>
		If you are considering LASIK and worried that something could go wrong, you might take comfort in knowing that it's very rare for complications from this procedure to cause permanent, significant vision loss. Also, many complications can be resolved through laser re-treatment.
	</p>
	<p>
		Selecting the right eye surgeon probably is the single most important step you can take to decrease any risks associated with LASIK. An experienced, reputable surgeon will make sure you are a good candidate for LASIK before a procedure is recommended. And if problems develop during or after the procedure, the surgeon should work closely with you to resolve them.
	</p>
	<b>
		How common are LASIK complications?
	</b>
	<p>
		Public confidence in the LASIK procedure has grown in recent years because of a solid success rate involving millions of successful procedures in the United States. With increasingly sophisticated technology used for the procedure, most LASIK outcomes these days are very favorable.
	</p>
	<p>
		The U.S. military also has adopted the use of refractive surgery including LASIK to decrease reliance of troops on prescription eyewear. In a study of more than 16,000 U.S. Army personnel who underwent refractive surgery from 2000 through 2003, 86 percent achieved 20/20 or better uncorrected vision and 98 percent achieved 20/40 or better (the legal requirement for driving without glasses in most states).
	</p>
	<p>
		"Reports of night vision difficulties, LASIK flap dislocation, and dry eye are infrequent, and do not seem to have a significant negative impact on military operations or individual readiness," researchers who studied these outcomes concluded in the February 2005 issue of <i>Ophthalmology</i>.
	</p>
	<b>
		LASIK complication rates
	</b>
	<p>
		Experienced LASIK surgeons report that serious complication rates can be held well below 1 percent if surgical candidates are screened and carefully selected. You may be eliminated as a candidate, for example, if you are pregnant or have certain conditions such as diabetes that may affect how well your eyes heal after LASIK. Discuss any health conditions you have with your eye doctor during your LASIK consultation or pre-operative exam.
	</p>
	<p>
		Large pupil sizes also might be risk factors for LASIK complications, because pupils in dark conditions could expand beyond the area of the eye that was treated. Again, make sure you discuss any concerns about these or other matters with your eye surgeon.
	</p>
	<b>
		Other considerations about LASIK risks
	</b>
	<p>
		While the great majority of LASIK outcomes are favorable, there is still that fraction of less than 1 percent of people who experience sometimes serious and ongoing vision problems following LASIK. No surgical procedure is ever risk-free.
	</p>
	<p>
		Some LASIK patients with excellent vision based on eye chart testing still can have bothersome side effects. For example, it’s possible a patient may see 20/20 or better with uncorrected vision after LASIK but still experience glare or halos around lights at night.
	</p>
	<p>
		When you sign the LASIK consent form prior to surgery, you should do so with a full understanding that, even in the best of circumstances, there is a slight chance something unintended could occur. Even so, most – but not all – problems eventually can be resolved.
	</p>
	<b>
		Common LASIK complications
	</b>
	<p>
		When LASIK complications occur, they are sometimes associated with the hinged flap that’s created in the cornea (the clear front covering of the eye) in the first step of the LASIK procedure. This flap lifted prior to re-shaping the underlying cornea with a laser, and is then replaced to form a natural bandage.
	</p>
	<p>
		If the LASIK flap is not made correctly, it may fail to adhere correctly to the eye's surface. The flap also might be cut too thinly or thickly. After the flap is placed back on the eye's surface, it might begin to wrinkle. These types of flap complications can lead to an irregularly-shaped eye surface.
	</p>
	<p>
		Studies indicate that flap complications occur in from 0.3 percent to 5.7 percent of LASIK procedures, according to the April 2006 issue of <i>American Journal of Ophthalmology</i>. But inexperienced surgeons definitely contribute to the higher rates of flap complications. Again, remember that you can improve your odds of avoiding LASIK risks by selecting a reputable, experienced eye surgeon.
	</p>
	<p>
		Some problems associated with LASIK flap complications include:
	</p>
	<ul type="disc">
		<li>
			<b>Irregular astigmatism</b> can result from a less than smooth corneal surface. Irregular astigmatism also can occur from laser correction that is not centered properly on the eye. Resulting symptoms may include double vision or "ghost images." In these cases, the eye may need re-treatment (also called an “enhancement”).
		</li>
		<li>
			<b>Diffuse Lamellar Keratitis (DLK)</b>, also nicknamed "Sands of the Sahara," is inflammation that can occur under the LASIK flap after surgery. In rare cases, DLK leads to corneal scarring. Potentially permanent vision loss can occur without prompt treatment with therapies such as antibiotics and topical steroids. The flap also might need to be lifted and rinsed to remove inflammatory cells and prevent tissue damage.
		</li>
		<li>
			<b>Ectasia (or keratectasia)</b> is bulging of the cornea that can occur from a flap being cut too deeply, when too much tissue is removed from the cornea during LASIK, or when the cornea was initially weakened as evidenced from cornea topography mapping prior to LASIK. Resulting distorted vision likely cannot be corrected with laser enhancement, and rigid contact lenses or <a href="http://www.allaboutvision.com/conditions/inserts.htm" target="_blank">corneal implants</a> (Intacs) may be prescribed to hold the cornea in place.
		</li>
	</ul>
	<p>
		Other, more commonly reported LASIK complications include:
	</p>
	<ul type="disc">
		<li>
			<b>Dry Eye:</b> Almost half of all patients report problems with dry eye in the first six months following LASIK, according to the April 2006 issue of American Journal of Ophthalmology. These complaints appear related to reduced sensitivity of the eye's surface immediately following the procedure. If you have this problem, temporary remedies such as artificial tears or prescription dry eye medication may be needed along with oral flaxseed oil capsules. After about six months to a year, however, most of these types of complaints disappear when healing of the eye is complete. People who already have severe dry eye usually are eliminated as LASIK candidates.
		</li>
		<li>
			<b>Significant Undercorrection, Overcorrection, or Regression:</b> An overcorrection or undercorrection of your vision problem means your vision may remain slightly blurred from residual nearsightedness, farsightedness or astigmatism. Regression is when your eyesight is optimal at first after LASIK, but then begins to deteriorate over time (due to a return of some nearsightedness, for example). These problems can usually be corrected with an enhancement LASIK procedure.
		</li>
		<li>
			<b>Eye Infection or Irritation:</b> In some rare cases, you may develop an eye infection, inflammation, or irritation that requires treatment with eye drops containing antibiotics or anti-inflammatory medication such as steroids.
		</li>
	</ul>
	<b>
		Vision changes unrelated to LASIK
	</b>
	<p>
		If you have LASIK in your 20s or 30s, be aware that your reading vision will change as you get older. This has nothing to do with your LASIK surgery – it’s caused by a normal age-related loss of focusing ability called presbyopia. Because of presbyopia, most LASIK patients (like anyone else who sees well without glasses in younger years) will need reading glasses after age 40.
	</p>
	<p>
		For more information on <a href="http://www.allaboutvision.com/visionsurgery/lasik_complication_1.htm" target="_blank">LASIK risks</a> visit <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.
	</p>
	<p class="credits">
		Article ©2012 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.
	</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
<li><a href="/articles/visionsurgery-inlays-onlays">Corneal Inlays and Onlays</a></li>
<li><a href="/articles/visionsurgery-corneal-transplant">Corneal Transplant</a></li>
<li><a href="/articles/visionsurgery-basics">Corrective Eye Surgery Basics</a></li>
<li><a href="/articles/visionsurgery-lasik">LASIK</a></li>
<li><a href="/articles/visionsurgery-lasik-criteria">LASIK - Criteria for Success</a></li>
<li><a href="/articles/visionsurgery-presbyopia" class="last">Surgery for Presbyopia</a></li>
</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<include href="gui/sidebar.html" />	
</aside>

