<section>
<article class="full">
	<h1>
		Lens Options for Eyeglasses
	</h1>
	<p>
		When it comes to choosing eyeglass lenses, it’s no longer a simple choice of “glass or plastic?” Let’s look at your many options in eyeglass lenses in detail:
	</p>
	<b>
		Want thinner, lighter lenses? Choose a high index lens material.
	</b>
	<p>
		Nearly everyone can benefit from thinner, lighter lenses. High index lenses can be up to 50% thinner than regular glass or plastic lenses, and they’re usually much lighter, too.
	</p>
	<p>
		Though these lenses are especially beneficial if you have a strong eyeglasses prescription, they can make a noticeable difference in the appearance of virtually any pair of glasses. High index lenses bend light more efficiently than regular glass or plastic lenses, so less lens material is required to correct your vision.
	</p>
	<p>
		Various high index lenses are available today in different price points based on how much thinner they are compared to regular plastic lenses. The lenses are classified by their “index of refraction” (or “refractive index”).
	</p>
	<p>
		Generally, lenses with a higher index of refraction will be thinner (and usually more expensive) than lenses with a lower index. The index of refraction of regular plastic lenses is 1.50. The refractive index of high index plastic lenses ranges from 1.53 to 1.74. Those in the range of 1.53 to 1.59 are about 20% thinner than regular plastic lenses, whereas 1.74 high index lenses are about half the thickness of regular plastic lenses.
	</p>
	<p>
		Most popular lens designs (single vision, bifocal, progressive, photochromic, etc.) come in high-index materials, and your doctor or optician will know which ones are available in your prescription. Bifocal and trifocal high index lenses are also available, though the selection is more limited.
	</p>
	<p>
		<i>Note: High index lenses reflect more light than regular glass or plastic lenses, so anti-reflective (AR) coating is highly recommended for these lenses (see below).</i>
	</p>
	<b>
		Slim down with aspheric lenses
	</b>
	<p>
		To make high index lenses even more attractive, most of them have an “aspheric” design. This means that instead of having a round (or “spherical”) curve on the front surface, these lenses have a curve that gradually changes from the center to the lens to the periphery. This makes aspheric lenses noticeably flatter for a slimmer, more attractive lens profile.
	</p>
	<p>
		Though aspheric lenses offer advantages for all prescriptions, they are particularly beneficial if you are farsighted. Aspheric lenses greatly reduce the magnified, “bug-eye” look caused by regular, highly curved lenses for farsightedness, and they greatly reduce the “bulge” of the lenses from the frame. And because they have a slim profile, aspheric lenses have less lens mass, making them much lighter. Aspheric lenses also provide superior peripheral vision compared to conventional lenses.
	</p>
	<p>
		<i>Note: Because they have flatter curves than regular lenses, aspheric lenses may cause more noticeable reflections. Anti-reflective (AR) coating is recommended for these lenses (see below).</i>
	</p>
	<b>
		Polycarbonate and Trivex lenses: Tough as nails
	</b>
	<p>
		Polycarbonate and Trivex lenses are special high index lenses that offer superior impact resistance. These lenses are up to 10 times more impact resistant than regular plastic lenses, making them an ideal choice for children’s eyewear, safety glasses, and for anyone with an active lifestyle who wants a thinner, lighter, safer lens.
	</p>
	<p>
		Polycarbonate lenses have a refractive index of 1.59, making them 20% to 25% thinner than regular plastic lenses. They are also up to 30% lighter than regular plastic lenses, making them a good choice for anyone who is sensitive to the weight of eyeglasses on their nose.
	</p>
	<p>
		Trivex lenses may be slightly thicker than polycarbonate lenses, but they provide comparable impact resistance and, like polycarbonate lenses, they block 100% of the sun’s harmful UV rays.
	</p>
	<b>
		AR coating: Better vision, better appearance
	</b>
	<p>
		All eyeglass lenses reflect some light, reducing the amount of light that enters the eye to form visual images. This can have an impact on vision, especially under low-light conditions, like when driving at night. Lens reflections can also cause glare, further reducing vision in these situations.
	</p>
	<p>
		The amount of light reflected depends on the lens material. Conventional glass or plastic lenses reflect about 8% of incident light, so only 92% of available light enters the eye for vision. Thinner, lighter lenses made of high index materials reflect up to 50% more light than regular glass or plastic lenses (up to 12% of available light).
	</p>
	<p>
		Anti-reflective (AR) coating reduces lens reflections and allows more light to enter the eye for better night vision. Regardless of the lens material, eyeglass lenses with AR coating transmit over 99% of available light to the eye.
	</p>
	<p>
		By eliminating surface reflections, anti-reflective coating also makes your lenses nearly invisible. This greatly improves the appearance of your eyewear and allows others to see your eyes, not the reflections in your glasses.
	</p>
	<p>
		When cleaning lenses with anti-reflective coating, be sure to use the products recommended by your optician. Because AR coating eliminates reflections that can hide small scratches, you’ll want to take care not to scratch AR-coated lenses, as scratches on these lenses may be more visible than scratches on an uncoated lens.
	</p>
	<b>
		Scratch-resistant coatings
	</b>
	<p>
		No eyeglass lens material – not even glass – is scratch-proof. However, a lens that is treated front and back with a clear, hard coating does become more resistant to scratching, whether it's from dropping your glasses on the floor or occasionally cleaning them with a paper towel. Kids' lenses, especially, benefit from a scratch-resistant hard coat.
	</p>
	<p>
		Nearly all high index lenses (including polycarbonate) come with a factory-applied scratch-resistant coating for added durability. This coating is optional for regular plastic lenses. However, to safeguard your investment in your eyewear, scratch-resistant coating should be considered for all eyeglass lenses. The only exception is glass lenses, which are naturally hard and scratch-resistant.
	</p>
	<p>
		To further protect your eyeglasses from scratches, keep your glasses in a protective case when you’re not wearing them. Also, never clean your lenses without first rinsing them with a cleaning solution or water. Rubbing a dry, dusty or dirty lens with a cleaning cloth or towel can cause scratches, even on lenses with a scratch-resistant coating.
	</p>
	<b>
		Ultraviolet (UV) treatment
	</b>
	<p>
		Just as you use sunscreen to keep the sun's UV rays from harming your skin, UV treatment in eyeglass lenses blocks those same rays from damaging your eyes. Overexposure to ultraviolet light is thought to be a cause of cataracts, retinal damage and other eye problems.
	</p>
	<p>
		Most high index lenses have 100% UV protection built-in. But with regular plastic lenses, a lens treatment is required for these lenses to block all UV rays. This UV treatment does not change the appearance of the lenses and is quite inexpensive.
	</p>
	<b>
		Photochromic lenses: Right in any light
	</b>
	<p>
		Photochromic lenses are convenient indoor-outdoor eyeglass lenses that automatically darken to a sunglass shade outside when exposed to sunlight, and then quickly return to a clear state indoors. These lenses also provide 100% protection from the sun’s UV rays, and are available in a wide variety of lens materials and designs, including bifocal and progressive lenses.
	</p>
	<p>
		The amount of darkening that most photochromic lenses undergo depends on how much UV radiation they are exposed to. As a general rule, these lenses won't get as dark behind the windshield of your car or truck because the glass blocks out much of the sun’s UV rays that cause the lenses to change color.
	</p>
	<b>
		We’re here to help!
	</b>
	<p>
		With so many new lens products available, it’s hard to know all your options and decide which lenses are best for you. Rely on our expertise to make selecting your eyeglasses easy and fun. Our trained staff welcomes the opportunity to help you find the perfect eyewear for your personal style and vision requirements.
	</p>
	<p>
		For more information on <a href="http://www.allaboutvision.com/lenses/coatings.htm" target="_blank">lens coatings</a> visit <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.
	</p>
	<p class="credits">
		Article ©2012 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.
	</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
<li><a href="/articles/eyeglasses-frames-materials">Eyeglass Frame Materials</a></li>
<li><a href="/articles/eyeglasses-men">Men's Eyeglass Frames</a></li>
<li><a href="/articles/eyeglasses-specialty">Specialty Eyewear</a></li>
<li><a href="/articles/eyeglasses-basics">The Basics of Eyeglasses</a></li>
<li><a href="/articles/eyeglasses-women" class="last">Women's Eyeglass Frames</a></li>
</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<include href="gui/sidebar.html" />	
</aside>

