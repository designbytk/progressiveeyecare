<section>
<article class="full">
	<h1>
		How Your Vision Changes as You Age
	</h1>
	<p>
		Just as our physical strength decreases with age, our eyes also exhibit an age-related decline in performance – particularly as we reach our 60s and beyond.
	</p>
	<p>
		Some age-related eye changes, such as presbyopia, are perfectly normal and don't signify any sort of disease process. While cataracts can be considered an age-related disease, they are extremely common among seniors and can be readily corrected with cataract surgery.
	</p>
	<p>
		Some of us, however, will experience more serious age-related eye diseases that have greater potential for affecting our quality of life as we grow older. These conditions include glaucoma, macular degeneration and diabetic retinopathy.
	</p>
	<b>
		When do age-related vision changes occur?
	</b>
    <b>Presbyopia.</b>
	<p>
		After you pass the milestone age of 40, you'll notice it's more difficult to focus on objects up close. This normal loss of focusing ability is called presbyopia, and is due to hardening of the lens inside your eye.
	</p>
	<p>
		For a time, you can compensate for this decline in focusing ability by just holding reading material farther away from your eyes. But eventually, you’ll need reading glasses, multifocal contact lenses or multifocal eyeglasses. Some corrective surgery options for presbyopia also are available, such as monovision LASIK and conductive keratoplasty (CK).
	</p>
	<b>
		Cataracts.
	</b>
	<p>
		Even though cataracts are considered an age-related eye disease, they are so common among seniors that they can also be classified as a normal aging change. According to Mayo Clinic, about half of all 65-year-old Americans have some degree of cataract formation in their eyes. As you enter your 70s, the percentage is even higher. It's estimated that by 2020 more than 30 million Americans will have cataracts.
	</p>
	<p>
		Thankfully, modern cataract surgery is extremely safe and so effective that 100% of vision lost to cataract formation usually is restored. If you are noticing vision changes due to cataracts, don't hesitate to discuss symptoms with your eye doctor. It's often better to have cataracts removed before they advance too far. Also, multifocal lens implants are now available. These advanced intraocular lenses (IOLs) potentially can restore all ranges of vision, thus reducing your need for reading glasses as well as distance glasses after cataract surgery.
	</p>
	<b>
		Major age-related eye diseases
	</b><b><a href="http://www.allaboutvision.com/conditions/amd.htm" target="_blank">Macular degeneration.</a></b>
	<p>
		Macular degeneration (also called age-related macular degeneration or AMD) is the leading cause of blindness among American seniors. According to the National Eye Institute (NEI), macular degeneration affects more than 1.75 million people in the United States. The U.S. population is aging rapidly, and this number is expected to increase to almost three million by 2020. Currently, there is no cure for AMD, but medical treatment may slow its progression or stabilize it.
	</p>
	<b>
		<b><a href="http://www.allaboutvision.com/conditions/glaucoma.htm" target="_blank">Glaucoma.</a></b>
	</b>
	<p>
		Your risk of developing glaucoma increases with each decade after age 40 – from around 1% in your 40s to up to 12% in your 80s. The number of Americans with glaucoma is expected to increase by 50% (to 3.6 million) by the year 2020. If detected early enough, glaucoma can often be controlled with medical treatment or surgery and vision loss can be prevented.
	</p>
	<b>
		<b><a href="http://www.allaboutvision.com/conditions/diabetic.htm" target="_blank">Diabetic retinopathy.</a></b>
	</b>
	<p>
		According to the NEI, approximately 10.2 million Americans over age 40 are known to have diabetes. Many experts believe that up to 30% of people who have diabetes have not yet been diagnosed. Among known diabetics over age 40, NEI estimates that 40% have some degree of diabetic retinopathy, and one of every 12 people with diabetes in this age group has advanced, vision-threatening retinopathy. Controlling the underlying diabetic condition in its early stages is the key to preventing vision loss.
	</p>
	<b>
		How aging affects other eye structures
	</b>
	<p>
		While normally we think of aging as it relates to conditions such as presbyopia and cataracts, more subtle changes in our vision and eye structures also take place as we grow older. These changes include:
	</p>
	<ul type="disc">
		<li>
			<b>Reduced pupil size.</b>As we age, muscles that control our pupil size and reaction to light lose some strength. This causes the pupil to become smaller and less responsive to changes in ambient lighting. Because of these changes, people in their 60s need three times more ambient light for comfortable reading than those in their 20s. Also, seniors are more likely to be dazzled by bright sunlight and glare when emerging from a dimly lit building such as a movie theater. Eyeglasses with photochromic lenses and anti-reflective coating can help reduce this problem.
		</li>
		<li>
			<b>Dry eyes.</b>As we age, our bodies produce fewer tears. This is particularly true for women after menopause. If you begin to experience burning, stinging or other eye discomfort related to dry eyes, use artificial tears as needed throughout the day for comfort, or consult your eye doctor for other options such as prescription dry eye medications.
		</li>
		<li>
			<b>Loss of peripheral vision.</b>Aging also causes a normal loss of peripheral vision, with the size of our visual field decreasing by approximately one to three degrees per decade of life. By the time you reach your 70s and 80s, you may have a peripheral visual field loss of 20 to 30 degrees.
		</li>
	</ul>
	<p>
		Because the loss of visual field increases the risk for automobile accidents, make sure you are more cautious when driving. To increase your range of vision, turn your head and look both ways when approaching intersections.
	</p>
	<ul type="disc">
		<li>
			<b>Decreased color vision.</b>Cells in the retina that are responsible for normal color vision decline in sensitivity as we age, causing colors to become less bright and the contrast between different colors to be less noticeable. In particular, blue colors may appear faded or "washed out." While there is no treatment for this normal, age-related loss of color perception, you should be aware of this loss if your profession (for example, artist, seamstress or electrician) requires fine color discrimination.
		</li>
		<li>
			<b>Vitreous detachment.</b>As we age, the gel-like vitreous inside the eye begins to liquefy and pull away from the retina, causing "spots and floaters" and (sometimes) flashes of light. This condition, called vitreous detachment, is usually harmless. But floaters and flashes of light can also signal the beginning of a retinal detachment – a serious problem that can cause blindness if not treated immediately. If you experience flashes and floaters, see your eye doctor immediately to determine the cause.
		</li>
	</ul>
	<b>
		What you can do about age-related vision changes
	</b>
	<p>
		A healthy diet and wise lifestyle choices – including exercising regularly, maintaining a healthy weight, reducing stress and not smoking – are your best natural defenses against vision loss as you age. Also, have regular eye exams with a caring and knowledgeable optometrist or ophthalmologist.
	</p>
	<p>
		Be sure to discuss with your eye doctor all concerns you have about your eyes and vision. Tell them about any history of eye problems in your family and any health problems you may have. Also, let your eye doctor know about any medications you take, including non-prescription vitamins, herbs and supplements.
	</p>
	<p>
		For more information on how <a href="http://www.allaboutvision.com/over60/vision-changes.htm" target="_blank">vision changes with age</a> visit <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.
	</p>
	<p class="credits">
		Article ©2012 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.
	</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
<li><a href="/articles/over60-protect">Eight Ways To Protect Your Eyesight</a></li>
<li><a href="/articles/over60-coping" class="last">Tips for Coping With Vision Loss</a></li>
</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<include href="gui/sidebar.html" />	
</aside>

