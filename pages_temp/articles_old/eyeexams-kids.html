<section>
<article class="full">
	<h1>
		Eye Exams for Children
	</h1>
	<p>
		As a parent, you may wonder whether your pre-schooler has a vision problem or when a first eye exam should be scheduled.
	</p>
	<p>
		Eye exams for children are extremely important. Experts say 5 percent-10 percent of pre-schoolers and 25 percent of school-aged children have vision problems. Early identification of a child's vision problem is crucial because, if left untreated, some childhood vision problems can cause permanent vision loss.
	</p>
	<b>
		When should kids have their eyes examined?
	</b>
	<p>
		According to the American Optometric Association (AOA), infants should have their first comprehensive eye exam at 6 months of age. Children then should receive additional eye exams at 3 years of age, and just before they enter kindergarten or the first grade at about age 5 or 6.
	</p>
	<p>
		For school-aged children, the AOA recommends an eye exam every two years if no vision correction is required. Children who need eyeglasses or contact lenses should be examined annually or according to their eye doctor’s recommendations.
	</p>
	<p>
		Early eye exams also are important because children need the following basic visual skills for learning:
	</p>
	<ul type="disc">
		<li>Near vision
		</li>
		<li>Distance vision
		</li>
		<li>Eye teaming (binocularity) skills
		</li>
		<li>Eye movement skills
		</li>
		<li>Focusing skills
		</li>
		<li>Peripheral awareness
		</li>
		<li>Eye/hand coordination
		</li>
	</ul>
	<p>
		Because of the importance of good vision for learning, some states require an eye exam for all children entering school for the first time.
	</p>
	<b>
		Scheduling your child’s eye exam
	</b>
	<p>
		Your family doctor or pediatrician likely will be the first medical professional to examine your child's eyes. If eye problems are suspected during routine physical examinations, a referral might be made to an ophthalmologist or optometrist for further evaluation. Eye doctors have specific equipment and training to help them detect and diagnose potential vision problems.
	</p>
	<p>
		When scheduling an eye exam, choose a time when your child is usually alert and happy. Specifics of how eye exams are conducted depend on your child's age, but an exam generally will involve a case history, vision testing, determination of whether eyeglasses are needed, testing of eye alignment, an eye health examination and a consultation with you regarding the findings.
	</p>
	<p>
		After you’ve made the appointment, you may be sent a case history form by mail, or you may be given one when you check in at the doctor's office. The case history form will ask about your child's birth history (also called perinatal history), such as birth weight and whether or not the child was full-term. Your eye doctor also may ask whether complications occurred during the pregnancy or delivery. The form will also inquire about your child's medical history, including current medications and past or present allergies.
	</p>
	<p>
		Be sure to tell your eye doctor if your child has a history of prematurity, has delayed motor development, engages in frequent eye rubbing, blinks excessively, fails to maintain eye contact, cannot seem to maintain a gaze (fixation) while looking at objects, has poor eye tracking skills or has failed a pediatrician or pre-school vision screening.
	</p>
	<p>
		Your eye doctor will also want to know about previous ocular diagnoses and treatments involving your child, such as possible surgeries and glasses or contact lens wear. Be sure you inform your eye doctor if there is a family history of eye problems requiring vision correction, such as nearsightedness or farsightedness, misaligned eyes (strabismus) or amblyopia (“lazy eye”).
	</p>
	<b>
		Eye testing for infants
	</b>
	<p>
		It takes some time for a baby’s vision skills to develop. To assess whether your infant's eyes are developing normally, your eye doctor may use one or more of the following tests:
	</p>
	<ul type="disc">
		<li>
			<b>Tests of pupil responses</b> evaluate whether the eye's pupil opens and closes properly in the presence or absence of light.
		</li>
		<li>
			<b>“Fixate and follow” testing</b> determines whether your baby can fixate on an object (such as a light) and follow it as it moves. Infants should be able to perform this task quite well by the time they are 3 months old.
		</li>
		<li>
			<b>Preferential looking</b> involves using cards that are blank on one side with stripes on the other side to attract the gaze of an infant to the stripes. In this way, vision capabilities can be assessed.
		</li>
	</ul>
	<b>
		Eye testing for pre-school children
	</b>
	<p>
		Pre-school children can have their eyes thoroughly tested even if they don’t yet know the alphabet or are too young or too shy to answer the doctor’s questions. Some common eye tests used specifically for young children include:
	</p>
	<ul type="disc">
		<li>
			<b>LEA Symbols</b> for young children are similar to regular eye tests using charts with letters, except that special symbols in these tests include an apple, house, square and circle.
		</li>
		<li>
			<b>Retinoscopy</b> is a test that involves shining a light into the eye to observing how it reflects from the retina (the light-sensitive inner lining of the back of the eye). This test helps eye doctors determine the child's eyeglass prescription.
		</li>
		<li>
			<b>Random Dot Stereopsis</b> uses dot patterns to determine how well the two eyes work as a team.
		</li>
	</ul>
	<b>
		Eye and vision problems that affect children
	</b>
	<p>
		Besides looking for nearsightedness, farsightedness and astigmatism (refractive errors), your eye doctor will be examining your child’s eyes for signs of these eye and vision problems commonly found in young children:
	</p>
	<ul type="disc">
		<li>
			<b>Amblyopia.</b> Also commonly called “lazy eye,” this is decreased vision in one or both eyes despite the absence of any eye health problem or damage. Common causes of amblyopia include strabismus (see below) and a significant difference in the refractive errors of the two eyes. Treatment of amblyopia may include patching the dominant eye to strengthen the weaker eye.
		</li>
		<li>
			<b>Strabismus.</b> This is misalignment of the eyes, often caused by a congenital defect in the positioning or strength of muscles that are attached to the eye and which control eye positioning and movement. Left untreated, strabismus can cause amblyopia in the misaligned eye. Depending on its cause and severity, surgery may be required to treat strabismus.
		</li>
		<li>
			<b>Convergence insufficiency.</b> This is the inability to keep the eye comfortably aligned for reading and other near tasks. Convergence insufficiency can often be successfully treated with vision therapy, a specific program of eye exercises.
		</li>
		<li>
			<b>Focusing problems.</b> Children with focusing problems (also called accommodation problems) may have trouble changing focus from distance to near and back again (accommodative infacility) or have problems maintaining adequate focus for reading (accommodative insufficiency). These problems often can be successfully treated with vision therapy.
		</li>
		<li>
			<b>Eye teaming problems.</b> Many eye teaming (binocularity) problems are more subtle than strabismus. Deficiencies in eye teaming skills can cause problems with depth perception and coordination.
		</li>
	</ul>
	<b>
		Vision and learning
	</b>
	<p>
		Experts say that 80% of what your child learns in school is presented visually. Undetected vision problems can put them at a significant disadvantage. Be sure to schedule a complete eye exam for your child prior to the start of school.
	</p>
	<p>
		For more information on <a href="http://www.allaboutvision.com/eye-exam/children.htm" target="_blank">children’s eye exams</a> visit <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.
	</p>
	<p class="credits">
		Article ©2012 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.
	</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
	<ul>
    <li><a href="/articles/eyeexams-preparing">Preparing for an Eye Exam</a></li>
    <li><a href="/articles/eyeexams-importance">Why Are Eye Exams Important?</a></li>
    <li><a href="/articles/eyeexams-expectations" class="last">Your Comprehensive Eye Exam</a></li>
</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<include href="gui/sidebar.html" />	
</aside>
