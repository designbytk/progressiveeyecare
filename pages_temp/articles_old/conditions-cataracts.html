<section>
<article class="full">
	<h1>
		Cataracts
	</h1>
	<p>
		A cataract is a clouding of the eye's natural lens, which lies behind the iris and the pupil. The lens works much like a camera lens, focusing light onto the <a href="http://www.allaboutvision.com/conditions/cataracts.htm" target="_blank">retina</a> at the back of the eye. The lens also adjusts the eye's focus, letting us see things clearly both up close and far away.
	</p>
	<p>
		The lens is mostly made of water and protein. The protein is arranged in a precise way that keeps the lens clear and allows light to pass through it. But as we age, some of the protein may clump together and start to cloud a small area of the lens. This is a cataract, and over time, it may grow larger and cloud more of the lens, making it harder to see.
	</p>
	<p>
		Most cataracts occur gradually as we age and don’t become bothersome until after age 55. However, cataracts can also be present at birth (congenital cataracts) or occur at any age as the result of an injury to the eye (traumatic cataracts). Cataracts can also be caused by diseases such as diabetes or can occur as the result of long-term use of certain medications, such as steroids.
	</p>
	<b>
		Cataract signs and symptoms
	</b>
	<p>
		A cataract starts out small, and at first has little effect on your vision. You may notice that your vision is blurred a little, like looking through a cloudy piece of glass or viewing an impressionist painting. However, as cataracts worsen, you are likely to notice some or all of these problems:
	</p>
	<ul style="disc">
		<li>Blurred vision that cannot be corrected with a change in your glasses prescription.
		</li>
		<li>Ghost images or double vision in one or both eyes.
		</li>
		<li>Glare from sunlight and artificial light, including oncoming headlights when driving at night.
		</li>
		<li>Colors appear faded and less vibrant.
		</li>
	</ul>
	<b>
		What causes cataracts?
	</b>
	<p>
		No one knows for sure why the eye's lens changes as we age, forming cataracts. Researchers are gradually identifying factors that may cause cataracts, and gathering information that may help to prevent them.
	</p>
	<p>
		Many studies suggest that exposure to ultraviolet light is associated with cataract development, so eye care practitioners recommend wearing sunglasses and a wide-brimmed hat to lessen your exposure. Other types of radiation may also be causes. For example, a study conducted in Iceland suggests that airline pilots have a higher risk of developing a nuclear cataract than non-pilots, and that the cause may be exposure to cosmic radiation. A similar theory suggests that astronauts, too, are at greater risk of cataracts due to their higher exposure to cosmic radiation.
	</p>
	<p>
		Other studies suggest people with diabetes are at risk for developing a cataract. The same goes for users of steroids, diuretics and major tranquilizers, but more studies are needed to distinguish the effect of the disease from the consequences of the drugs themselves.
	</p>
	<p>
		Some eyecare practitioners believe that a diet high in antioxidants, such as beta-carotene (vitamin A), selenium and vitamins C and E, may forestall cataract development. Meanwhile, eating a lot of salt may increase your risk.
	</p>
	<p>
		Other risk factors for cataracts include cigarette smoke, air pollution and heavy alcohol consumption.
	</p>
	<b>
		Cataract treatment
	</b>
	<p>
		When symptoms of cataracts begin to appear, you may be able to improve your vision for a while using new glasses, stronger bifocals and greater light when reading. But when these remedies fail to provide enough benefit, it’s time for cataract surgery.
	</p>
	<p>
		Cataract surgery is very successful in restoring vision. In fact, it is the most frequently performed surgery in the United States, with nearly 3 million cataract surgeries done each year. More than 90% of people who have cataract surgery regain very good vision, somewhere between 20/20 and 20/40, and sight-threatening complications are relatively rare.
	</p>
	<p>
		During surgery, the surgeon will remove your clouded lens and replace it with a clear, plastic intraocular lens (IOL). New IOLs are being developed all the time to make the surgery less complicated for surgeons and the lenses more helpful to patients. Presbyopia-correcting IOLs not only improve your distance vision, but can decrease your reliance on reading glasses as well.
	</p>
	<p>
		If you need cataracts removed from both eyes, surgery usually will be done on only one eye at a time. An uncomplicated surgical procedure lasts only about 10 minutes. However, you may be in the outpatient facility for 90 minutes or longer because extra time will be needed for preparation and recovery.
	</p>
	<b>
		Presbyopia-correcting IOLs: Frequently asked questions
	</b>
	<p>
		If you need cataract surgery, you may have the option of paying extra for new presbyopia-correcting IOLs that potentially can restore a full range of vision without eyeglasses.
	</p>
	<p>
		Presbyopia-correcting IOLs are a relatively new option, so you may have questions such as:
	</p>
	<b>
		1. What are presbyopia-correcting IOLs?
	</b>
	<p>
		Presbyopia-correcting intraocular lenses (IOLs) are lens implants that can correct both distance and near vision, giving you greater freedom from glasses after cataract surgery than standard IOLs. They are available in two forms: multifocal lenses and accommodating lenses. Multifocal lenses are similar to multifocal contact lenses – they contain more than one lens power for different viewing distances. Accommodating IOLs have just one lens power, but the lens is mounted on flexible “legs” that allow the lens to move forward or backward within your eye in response to focusing effort to enable you to see clearly at a range of distances.
	</p>
	<b>
		2. Aren't presbyopia-correcting IOLs a lot more expensive? How much extra do I have to pay?
	</b>
	<p>
		Yes, presbyopia-correcting IOLs are more expensive than standard IOLs. Costs vary, depending on the lens used, but you can expect to pay up to $2,500 extra per eye. This added amount is usually not covered by Medicare or other health insurance policies, so it would be an “out-of-pocket” expense if you choose this advanced type of IOL for your cataract surgery.
	</p>
	<b>
		3. Why won't Medicare or health insurance cover the <i>full</i> cost of presbyopia-correcting IOLs?
	</b>
	<p>
		A multifocal or accommodating IOL is not considered medically necessary. In other words, Medicare or your insurance will pay only the cost of a basic IOL and accompanying cataract surgery. Use of a more expensive, presbyopia-correcting lens is considered an elective refractive procedure, a type of luxury, just as LASIK and PRK are refractive procedures that also typically are not covered by health insurance.
	</p>
	<b>
		4. Can my local cataract surgeon perform presbyopia-correcting surgery?
	</b>
	<p>
		Not all cataract surgeons use presbyopia-correcting IOLs for cataract surgery. Make sure your eye surgeon has experience with these lenses if you choose a multifocal or accommodating IOL. Studies have shown that surgeon experience is a key factor in successful outcomes, particularly in terms of whether you will need to wear eyeglasses following cataract surgery.
	</p>
	<b>
		5. Are any problems associated with presbyopia-correcting IOLs?
	</b>
	<p>
		At a 2007 American Society of Cataract and Refractive Surgery conference, some reports indicated that even experienced cataract surgeons needed to perform enhancements for 13% to 15% of cases involving use of presbyopia-correcting IOLs. Enhancements don't mean that the procedure itself was a failure, because you likely will see just fine with eyeglasses even if your outcome is less than optimal. But it’s possible you may need an additional surgical procedure (such as LASIK) to perfect your uncorrected vision after cataract surgery with a presbyopia-correcting IOL. Depending on the arrangement you make with your eye surgeon, you also may need to pay extra for any needed enhancements.
	</p>
	<p>
		For more information on <a href="http://www.allaboutvision.com/conditions/cataracts.htm" target="_blank">cataracts</a> visit <a href="http://www.allaboutvision.com/" target="_blank">AllAboutVision.com</a>.
	</p>
	<p class="credits">
		Article ©2012 Access Media Group LLC. All rights reserved. Reproduction other than for one-time personal use is strictly prohibited.
	</p>
</article>
</section>
<aside>
<div class="similar">
<h2><i class="icon-plus pull-left"></i>Similar Articles</h2>
<ul>
<li><a href="/articles/conditions-amblyopia">Amblyopia (Lazy Eye)</a></li>
<li><a href="/articles/conditions-astigmatism">Astigmatism</a></li>
<li><a href="/articles/conditions-blepharitis">Blepharitis</a></li>
<li><a href="/articles/conditions-cmv">CMV Retinitis</a></li>
<li><a href="/articles/cornea-transplant">Cornea Transplant</a></li>
<li><a href="/articles/conditions-cornea-transplant">Cornea Transplant (Conditions)</a></li>
<li><a href="/articles/conditions-diabetic">Diabetic Retinopathy</a></li>
<li><a href="/articles/conditions-dry-eyes">Dry Eye Syndrome</a></li>
<li><a href="/articles/conditions-allergies">Eye Allergies</a></li>
<li><a href="/articles/conditions-floaters">Floaters and Spots</a></li>
<li><a href="/articles/conditions-glaucoma">Glaucoma</a></li>
<li><a href="/articles/conditions-hyperopia">Hyperopia</a></li>
<li><a href="/articles/conditions-keratoconus">Keratoconus</a></li>
<li><a href="/articles/conditions-amd">Macular Degeneration</a></li>
<li><a href="/articles/conditions-myopia">Myopia</a></li>
<li><a href="/articles/conditions-ocular-hypertension">Ocular Hypertension</a></li>
<li><a href="/articles/conditions-pinguecula">Pingueculae</a></li>
<li><a href="/articles/conditions-pinkeye">Pink Eye (Conjunctivitis)</a></li>
<li><a href="/articles/conditions-presbyopia">Presbyopia</a></li>
<li><a href="/articles/conditions-ptosis">Ptosis</a></li>
<li><a href="/articles/conditions-retinal-detach">Retinal Detachment</a></li>
<li><a href="/articles/conditions-retinitis-pigmentosa">Retinitis Pigmentosa</a></li>
<li><a href="/articles/conditions-styes">Stye</a></li>
<li><a href="/articles/conditions-uveitis" class="last">Uveitis</a></li>
</ul>
<a href="/articles/" class="more">Even more</a>
</div>
<include href="gui/sidebar.html" />	
</aside>
